
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

/**
 * @author Brendan White
 */

package com.ctctlabs.quickview;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.http.auth.InvalidCredentialsException;
import org.apache.http.client.ClientProtocolException;

import com.ctctlabs.quickview.webservice.Contact;
import com.ctctlabs.quickview.webservice.ContactList;
import com.ctctlabs.quickview.webservice.ContactListIterator;
import com.ctctlabs.quickview.webservice.ModelObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

/** Activity to both add a new contact and edit an existing one. A contact can be
 *  added to multiple lists in this way.  **/
public class AddContactActivity extends Activity
								implements OnClickListener,
										   OnItemClickListener {
    
	//incoming intent from contact details
	static final String DETAILS_ACTION = 
			            "com.ctctlabs.quickview.ContactDetailsActivity.EDIT_CONTACT";
	
	//incoming intent from list details (add-to-list)
	final String ADD_TO_LIST_ACTION = 
						"com.ctctlabs.quickview.ListDetailsActivity.ADD_TO_LIST";
	//column inputs and corresponding values
	private EditText emailAddress;		private String emailValue = "";
	private EditText firstName;			private String firstNameValue = "";
	private EditText lastName;			private String lastNameValue = "";
	private EditText address1;			private String address1Value = "";
	private EditText address2;			private String address2Value = "";
	private EditText address3;			private String address3Value = "";
	private EditText city;				private String cityValue = "";
	private EditText state;				private String stateValue = "";
	private EditText postalCode;		private String postalCodeValue = "";
	private EditText homePhone;			private String homePhoneValue = "";
	private EditText workPhone;			private String workPhoneValue = "";
	private EditText jobTitle;			private String jobTitleValue = "";
	private EditText company;			private String companyValue = "";
	
	//the editable contact
	private Contact contact;
	private ArrayList<HashMap<String, String>> lists;
	
	//task to get ALL lists
	private GetListsTask mTask;
	private AddContactTask mAddTask;
	private UpdateContactTask mUpdateTask;
	
	//view to display contact lists
	private ListView mListView;
	
	private Button showLists;
	
	private ArrayList<String> selectedLists;
	
	/* Setup the views for the Activity */
	private void setupViews() {
        //setup the done button
		Button done = (Button)findViewById(R.id.btn_done_contacts);
        done.setOnClickListener(this);
        
        //setup the revert button
        Button revert = (Button)findViewById(R.id.btn_revert_contacts);
        revert.setOnClickListener(this);
        
        //setup the button to display the lists 
        showLists = (Button)findViewById(R.id.btn_lists_contacts);
        showLists.setOnClickListener(this);
        showLists.setText("Loading Lists...");
        showLists.setEnabled(false);
        
        //setup all the input objects
        emailAddress = (EditText)findViewById(R.id.et_emailaddress);
        firstName = (EditText)findViewById(R.id.et_firstname);
        lastName = (EditText)findViewById(R.id.et_lastname);
        address1 = (EditText)findViewById(R.id.et_address);
        address2 = (EditText)findViewById(R.id.et_address2);
        address3 = (EditText)findViewById(R.id.et_address3);
        city = (EditText)findViewById(R.id.et_city);
        state = (EditText)findViewById(R.id.et_state);
        postalCode = (EditText)findViewById(R.id.et_postalcode);
        homePhone = (EditText)findViewById(R.id.et_phonenumber);
        workPhone = (EditText)findViewById(R.id.et_workphone);
        jobTitle = (EditText)findViewById(R.id.et_jobtitle);
        company = (EditText)findViewById(R.id.et_company);
	}//end setupViews
	
	/* Fill the input fields with the values if any */
	private void setValues() {
        emailAddress.setText(emailValue);
        firstName.setText(firstNameValue);      
        lastName.setText(lastNameValue);        
        address1.setText(address1Value);        
        address2.setText(address2Value);       
        address3.setText(address3Value);       
        city.setText(cityValue);        
        state.setText(stateValue);       
        postalCode.setText(postalCodeValue);       
        homePhone.setText(homePhoneValue);       
        workPhone.setText(workPhoneValue);       
        jobTitle.setText(jobTitleValue);       
        company.setText(companyValue);
	}//end setValues
	
	/* For the selected contact, get all of the attributes available */
	private void getValues() {
		Contact contact = ContactDetailsActivity.contact;
		
		try {
			emailValue = (String) contact.getAttribute("EmailAddress");
			firstNameValue = (String) contact.getAttribute("FirstName");
			lastNameValue = (String) contact.getAttribute("LastName");
			address1Value = (String) contact.getAttribute("Addr1");
			address2Value = (String) contact.getAttribute("Addr2");
			address3Value = (String) contact.getAttribute("Addr3");
			cityValue = (String) contact.getAttribute("City");
			stateValue = (String) contact.getAttribute("StateCode");
			postalCodeValue = (String) contact.getAttribute("PostalCode");
			homePhoneValue = (String) contact.getAttribute("HomePhone");
			workPhoneValue = (String) contact.getAttribute("WorkPhone");
			jobTitleValue = (String) contact.getAttribute("JobTitle");
			companyValue = (String) contact.getAttribute("CompanyName");
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}//end getValues
	
	/* Gets the lists that a given contact is subscribed */
	private ArrayList<String> getSubscribedLists() {
		
		ArrayList<String> subscribed = new ArrayList<String>();
		try {
			ArrayList<ContactList> contactLists = 
				(ArrayList<ContactList>) contact.getAttribute("ContactLists");
			String listLink = "";
			for(ContactList contactList : contactLists) {
				listLink = (String) contactList.getAttribute("Link");
				subscribed.add(listLink);
			}//end for
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return subscribed;
	}//end getSubscribedLists
	
	/* Gets the currently selected lists */
	private ArrayList<ContactList> getSelectedLists() {
		ArrayList<ContactList> contactLists = new ArrayList<ContactList>();
		String selected = "";
		for(int i = 0; i < lists.size(); i++) {
			selected = lists.get(i).get("SELECTED");
			if(selected.equals("true")) {
				String listLink = lists.get(i).get("LINK");
				try {
					contactLists.add(Connection.getConn().getContactList(listLink));
				} catch (InvalidCredentialsException e) {
					e.printStackTrace();
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}//end if
		}//end for
		
		return contactLists.isEmpty() ? null : contactLists;
	}//end getSelectedLists
	
	/* RetrieveListsTask */
	public class GetListsTask extends AsyncTask<ArrayList, Integer, 
											    ArrayList<HashMap<String, String>>> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			lists = new ArrayList<HashMap<String,String>>();
		}//end onPreExecute

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(ArrayList... params) {
			
			ContactListIterator listIter = null;
			ArrayList<ModelObject> loadedLists = null;
			try {
				listIter = Connection.getConn().getContactLists();
				while(listIter.hasNextPage()) {
					listIter.loadNextPage();
				}//end while
				loadedLists = listIter.getLoadedEntries();
			} catch (InvalidCredentialsException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			HashMap<String, String> list;
			ArrayList<String> subscribed = params[0];
			
			//determine if edit
			String action = getIntent().getAction();
			boolean isEdit = false;
			if(action != null && (action.equals(DETAILS_ACTION) ||
					action.equals(ADD_TO_LIST_ACTION))) {
				isEdit = true;
			}//end if
			
			for(int i = 0; i < loadedLists.size(); i++) {
				String listName;
				try {
					listName = loadedLists.get(i).getAttribute("Name").toString();
					if(!(listName.contains("Active") ||
							listName.contains("Do Not Mail") ||
							listName.contains("Removed"))) {
						list = new HashMap<String, String>();
						String listLink = loadedLists.get(i).getAttribute("Link").toString();
						list.put("NAME", listName);
						list.put("LINK", listLink);
						if(isEdit && subscribed != null) {
							if(subscribed.contains(listLink)) {
								list.put("SELECTED", "true");
							} else {
								list.put("SELECTED", "false");
							}
						} else {
							list.put("SELECTED", "false");
						}
						lists.add(list);
					}//end if
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}//end for
			return lists;
		}//end doInBackground

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
	        unlockShowButton();
		}//end onPostExecute
	}//end GetListsTask
	
	/** ListsAdapter **/
	public class ListsAdapter extends BaseAdapter {

		private ArrayList<HashMap<String, String>> lists;
		private Context context;
		
		@SuppressWarnings("unchecked")
		public ListsAdapter(Context _context, ArrayList _lists) {
			super();
			this.lists = _lists;
			this.context = _context;
		}//end ListsAdapter constructor

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View mView = convertView;
			
			if(mView == null)
				mView = View.inflate(context, R.layout.checked_item, null);
			
			HashMap<String, String> list = lists.get(position);
			if(list != null) {
				String listName = list.get("NAME");
				String isSelected = list.get("SELECTED");
				
				CheckedTextView ctvChecked = 
								(CheckedTextView)mView.findViewById(android.R.id.text1);
				ctvChecked.setEllipsize(TruncateAt.END);
				ctvChecked.setText(listName);

				if(isSelected.equals("true")) {
					mListView.setItemChecked(position, true);
				}
			}//end outer if
			
			return mView;
		}//end getView
		
		public void refresh() {
			notifyDataSetChanged();
		}//end refresh

		@Override
		public int getCount() {
			return lists.size();
		}//end getCount

		@Override
		public Object getItem(int position) {
			return lists.get(position);
		}//end getItem

		@Override
		public long getItemId(int position) {
			return position;
		}//end getItemId
	}//end ListsAdapter
	
	/* AddContactTask */
	public class AddContactTask extends AsyncTask<ArrayList<ContactList>, Integer, 
												  String> {
		@Override
		protected String doInBackground(ArrayList<ContactList>... params) {
			
			//the selected contact lists to add the contact to
			ArrayList<ContactList> contactLists = params[0];
			
			//contact data to add
			HashMap<String, Object> contactData = new HashMap<String, Object>();
			
			//determine what input has been entered
			if(!emailAddress.getText().toString().equals(""))
				contactData.put("EmailAddress", emailAddress.getText().toString());
			if(!firstName.getText().toString().equals(""))
				contactData.put("FirstName", firstName.getText().toString());
			if(!lastName.getText().toString().equals(""))
				contactData.put("LastName", lastName.getText().toString());
			if(!address1.getText().toString().equals(""))
				contactData.put("Addr1", address1.getText().toString());
			if(!address2.getText().toString().equals(""))
				contactData.put("Addr2", address2.getText().toString());
			if(!address3.getText().toString().equals(""))
				contactData.put("Addr3", address3.getText().toString());
			if(!city.getText().toString().equals(""))
				contactData.put("City", city.getText().toString());
			if(!state.getText().toString().equals(""))
				contactData.put("StateCode", state.getText().toString());
			if(!postalCode.getText().toString().equals(""))
				contactData.put("PostalCode", postalCode.getText().toString());
			if(!homePhone.getText().toString().equals(""))
				contactData.put("HomePhone", homePhone.getText().toString());
			if(!workPhone.getText().toString().equals(""))
				contactData.put("WorkPhone", workPhone.getText().toString());
			if(!jobTitle.getText().toString().equals(""))
				contactData.put("JobTitle", jobTitle.getText().toString());
			if(!company.getText().toString().equals(""))
				contactData.put("CompanyName", company.getText().toString());
			
			contactData.put("ContactLists", contactLists);
				
			Contact contact = null;
			String contactId = null;
			try {
				contact = Connection.getConn().createContact(contactData);
				contact.commit();
				
				contactId = contact.hasAttribute("ContactId") ? 
						    contact.getAttribute("Link").toString() : 
						    null;
			} catch (InvalidCredentialsException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			//return the contact id, will be null if failed to add
			return contactId;
		}//end doInBackground

		@Override
		protected void onPostExecute(String contactId) {
			super.onPostExecute(contactId);
			dismissDialog(1);
			if(contactId != null) {
				String action = getIntent().getAction();
				if(action != null && action.equals(ADD_TO_LIST_ACTION)) {
					Intent intent = new Intent();
					intent.putExtra("EMAIL_ADDRESS", emailAddress.getText().toString());
					intent.putExtra("CONTACT_ID", contactId);
					setResult(Activity.RESULT_OK, intent);
				}//end if
				finish();
			}//end outer-if
		}//end onPostExecute
	}//end AddContactTask
	
	/* UpdateContactTask */
	public class UpdateContactTask extends AsyncTask<ArrayList<ContactList>, Integer, 
												  Boolean> {
		@Override
		protected Boolean doInBackground(ArrayList<ContactList>... params) {
			
			//the selected contact lists to add the contact to
			ArrayList<ContactList> contactLists = params[0];
			
			Contact contactToUpdate = contact;
			
			if(!emailAddress.getText().toString().equals(emailValue))
				contactToUpdate.setAttribute("EmailAddress", emailAddress.getText().toString());
			if(!firstName.getText().toString().equals(firstNameValue))
				contactToUpdate.setAttribute("FirstName", firstName.getText().toString());
			if(!lastName.getText().toString().equals(lastNameValue))
				contactToUpdate.setAttribute("LastName", lastName.getText().toString());
			if(!address1.getText().toString().equals(address1Value))
				contactToUpdate.setAttribute("Addr1", address1.getText().toString());
			if(!address2.getText().toString().equals(address2Value))
				contactToUpdate.setAttribute("Addr2", address2.getText().toString());
			if(!address3.getText().toString().equals(address3Value))
				contactToUpdate.setAttribute("Addr3", address3.getText().toString());
			if(!city.getText().toString().equals(cityValue))
				contactToUpdate.setAttribute("City", city.getText().toString());
			if(!state.getText().toString().equals(stateValue))
				contactToUpdate.setAttribute("StateCode", state.getText().toString());
			if(!postalCode.getText().toString().equals(postalCodeValue))
				contactToUpdate.setAttribute("PostalCode", postalCode.getText().toString());
			if(!homePhone.getText().toString().equals(homePhoneValue))
				contactToUpdate.setAttribute("HomePhone", homePhone.getText().toString());
			if(!workPhone.getText().toString().equals(workPhoneValue))
				contactToUpdate.setAttribute("WorkPhone", workPhone.getText().toString());
			if(!jobTitle.getText().toString().equals(jobTitleValue))
				contactToUpdate.setAttribute("JobTitle", jobTitle.getText().toString());
			if(!company.getText().toString().equals(companyValue))
				contactToUpdate.setAttribute("CompanyName", company.getText().toString());
			
			contactToUpdate.setAttribute("ContactLists", contactLists);
			
			try {
				contactToUpdate.commit();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return Boolean.TRUE;
		}//end doInBackground

		@Override
		protected void onPostExecute(Boolean wasUpdated) {
			super.onPostExecute(wasUpdated);
			dismissDialog(2);
			if(wasUpdated) {
				setResult(Activity.RESULT_OK, new Intent());
				finish();
			}//end if
		}//end onPostExecute
	}//end UpdateContactTask
	
	@Override
	public Object onRetainNonConfigurationInstance() {
		return lists;
	}//end onRetainNonConfigurationInstance
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //no title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        //set the content view and setup the views
        setContentView(R.layout.add_contact_view);
        setupViews();
        
        ArrayList<String> subscribed = null;
        String action = getIntent().getAction();
        if(action != null && action.equals(DETAILS_ACTION)) {
        	contact = ContactDetailsActivity.contact;
        	subscribed = getSubscribedLists();
        	getValues();
        	setValues();
        }//end if
        
        if(action != null && action.equals(ADD_TO_LIST_ACTION)) {
        	subscribed = new ArrayList<String>();
        	subscribed.add(getIntent()
            		  .getStringExtra(ListDetailsActivity.EXTRA_LIST_NAME));
        }//end if
		
		ArrayList passedData = (ArrayList) getLastNonConfigurationInstance();
		if(passedData == null) {
			mTask = new GetListsTask();
	        mTask.execute(subscribed);
		} else {
			unlockShowButton();
			lists = passedData;
		}//end if-else
    }//end onCreate
    
	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		
		case R.id.btn_done_contacts:
			if(!isValidInput()) 
				break;
			if(isAddActivity()){
				
				// permissions dialog should only be shown once, so use Preferences to check whether or not it's already been shown.
				SharedPreferences prefs = getSharedPreferences(getResources().getString(R.string.str_app_prefs_key), 0);
			    SharedPreferences.Editor prefsEditor = prefs.edit();
				if(prefs.getBoolean("haveShownContactPermissions", false) == false){
					
					// store that we're showing it, so we won't again.
					prefsEditor.putBoolean("haveShownContactPermissions", true);
					prefsEditor.commit();

					showPermissions();					
				} else {
					
					// skip straight to the progress dialog that kicks off the contact save
					startProgress(1);					
				}
			} else {
				showDialog(2);
				mUpdateTask = new UpdateContactTask();
				mUpdateTask.execute(getSelectedLists());
			}
			break;
			
		case R.id.btn_revert_contacts:
			setValues();
			break;	
			
		case R.id.btn_lists_contacts:
			showLists();
			break;
			
		}//end switch
	}
	
	private boolean isValidInput() {
		String email = emailAddress.getText().toString();
		if(email.equals("")) {
			QuickViewHelper.showDialog(this, getString(R.string.str_errortitle), 
					   "Please enter a valid email address for this contact.", false);
			return false;
		}//end if
		
		if(getSelectedLists() == null) {
			QuickViewHelper.showDialog(this, getString(R.string.str_errortitle), 
					   "Please subscribe this contact to at least one contact list.", false);
			return false;
		}
		return true;
	}//end isValidInput

	private boolean isAddActivity() {
		String action = getIntent().getAction();
		if(action != null && action.equals(DETAILS_ACTION)) {
			return false;
		} 
		return true;
	}//end isAddActivity
	
	/* Show the lists retrieved by GetListsTask */
	private void showLists() {
		//setup the listview to display the contact lists
		mListView = new ListView(this);
		mListView.setOnItemClickListener(this);
		mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		mListView.setClickable(true);
		mListView.setBackgroundColor(Color.WHITE);
		mListView.setCacheColorHint(0x00000000);
		mListView.setAdapter(new ListsAdapter(this, lists));
        
		//show the lists
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		builder.setTitle("Contact lists subscriptions");
		builder.setView(mListView);
		builder.setCancelable(true);
		builder.create().show();
	}//end showLists
	
	private void unlockShowButton() {
		showLists.setEnabled(true);
        showLists.setText("Contact lists subscriptions");
	}//end unlockShowButton
	
	private boolean showPermissions() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.permissions_title);
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setCancelable(false);
		builder.setPositiveButton(R.string.permissions_btn_agree, new DialogInterface.OnClickListener() {
		      public void onClick(DialogInterface dialog, int which) {
		          startProgress(1);
		        }}); 
		
		builder.setNegativeButton(R.string.permissions_btn_cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				return;
			}});
		
		String permissionsMessage = getResources().getString(R.string.permissions_body1) +
									getResources().getString(R.string.permissions_body2);
		builder.setMessage(permissionsMessage);
		builder.create().show();
		
		return false;
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		ProgressDialog mDialog = new ProgressDialog(this);
		mDialog.setIndeterminate(true);
		mDialog.setCancelable(false);
		if(id == 1) {
			mDialog.setMessage("Saving contact...");
			return mDialog;
		} else if(id == 2) {
			mDialog.setMessage("Updating contact...");
			return mDialog;
		}
		return super.onCreateDialog(id);
	}//end onCreateDialog

	private void startProgress(int id) {
		showDialog(id);
		mAddTask = new AddContactTask();
		mAddTask.execute(getSelectedLists());
	}//end startProgress

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, 
						    int position, long positionAsLong) {
		boolean selected = mListView.isItemChecked(position);
		mListView.setItemChecked(position, selected);
		if(selected) {
			lists.get(position).put("SELECTED", "true");
		} else {
			lists.get(position).put("SELECTED", "false");		
		}
	}//end onItemClick
}
