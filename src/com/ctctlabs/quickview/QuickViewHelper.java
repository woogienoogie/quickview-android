
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

/**
 * @author Brendan White
 */

package com.ctctlabs.quickview;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.TextView;

/** QuickViewHelper **/
public class QuickViewHelper {
	
	/* getLoadingview */
	public static View getLoadingView(Context context, String headingText) {
		View mView = View.inflate(context, R.layout.loading, null);
		if(headingText != null)
		{
			TextView heading = (TextView)mView.findViewById(R.id.tv_heading_loading);
			heading.setText(headingText);
			heading.setVisibility(View.VISIBLE);
		}//end if
		return mView;
	}//end getLoadingView
	
	public static void showDialog(Context context, String title,
								  String message, boolean isCancelable) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setCancelable(isCancelable);
		builder.setPositiveButton(R.string.str_positivebtntxt,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						return;
					}
				});
		builder.setMessage(message);
		builder.create().show();
	}//end showDialog
		
	public static void showLogOutDialog(Context context, final Activity activity) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(R.string.str_logouttitle);
		builder.setCancelable(false);
		builder.setPositiveButton(R.string.str_logoutbtntxt,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						clearDefaultUser(activity);
						activity.startActivity(new Intent(activity, LoginActivity.class));
						activity.finish();
						return;
					}
				});
		builder.setNegativeButton(R.string.str_negativebtntxt,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						return;
					}
				});
		builder.setMessage(R.string.str_logoutmsg);
		builder.create().show();
	}//end showLogOutDialog
	
	/* Convert CTCT timestamp to a date in the form "Jan 1, 2010" */
	public static String convertDate(String date) {
		String convertedDate = "";
		
		//from and to formats
		SimpleDateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat toFormat = new SimpleDateFormat("MMM dd yyyy");
		
		try {
			//convert the date
			convertedDate = toFormat.format(fromFormat.parse(date));
		} catch (ParseException e) {
			//there is a parsing problem, don't bother including the date
		}
		return convertedDate;
	}//end convertDate
	
	public static String getURLPath(String url) {
		String path = "";
		
		URL mURL = null;
		try {
			mURL = new URL(url);
			path = mURL.getPath();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (StringIndexOutOfBoundsException e) {
			e.printStackTrace();
		}//end try-catch
	
		return path;
	}//end getURLPath
	
	public static void clearDefaultUser(final Activity activity) {
		SharedPreferences prefs = activity.getSharedPreferences(activity.getResources().getString(R.string.str_app_prefs_key), 0);
		SharedPreferences.Editor prefsEditor = prefs.edit();
		prefsEditor.putString("defaultUser", null);
		prefsEditor.commit();
	}//end clearDefaultUser
	
	public static boolean checkConnection(Activity activity) {		
		
		ConnectivityManager connMan;
		NetworkInfo wifiInfo, mobileInfo;
		
		connMan = (ConnectivityManager) activity
				  .getSystemService(Context.CONNECTIVITY_SERVICE);
		
		wifiInfo = connMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		mobileInfo = connMan.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		
		if(wifiInfo.isConnected() || mobileInfo.isConnected()) {
			return true;
		} 

		return false;
	}//end checkConnection
	
	public static void showNoConnectionDialog(final Activity activity, 
											  final Handler tryAgainHandler,
											  final String username,
											  final String password) {
		final AlertDialog.Builder builder = 
			  new AlertDialog.Builder(activity);
		builder.setTitle(R.string.str_noconnectiontitle);
		builder.setCancelable(false);
		builder.setPositiveButton(R.string.str_noconnectionpositivebtntxt,
				new DialogInterface.OnClickListener() {	
				public void onClick(DialogInterface dialog, int which) {
						Bundle data = new Bundle();
						data.putString("username", username);
						data.putString("password", password);
						Message message = new Message();
						message.setData(data);
						tryAgainHandler.sendMessage(message);
						return;
					}
				});
		builder.setNegativeButton(R.string.str_negativebtntxt,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						return;
					}
				});
		builder.setMessage(R.string.str_noconnectionmsg);
		builder.create().show();
	}//end showLogOutDialog
}//end QuickViewHelper
