
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

/**
 * @author Brendan White
 */

package com.ctctlabs.quickview;

import java.io.IOException;
import org.apache.http.client.ClientProtocolException;
import android.os.AsyncTask;

abstract class LoginUserTask extends AsyncTask<String, Integer, Boolean> {

	// TODO put a valid API key here. If you don't have one, you can request one at developer.constantcontact.com .
	private final String API_KEY = "c1f17229-d561-4658-b09a-ebdcf57eeeec";
	
	protected String username;
	protected String password;

	/* Thread to log in the user with the supplied credentials */
	@Override
	protected Boolean doInBackground(String... params) {
		username = params[0];
		password = params[1];
		
		//create a new connection
		Connection conn = new Connection();
		
		// ensure the connection is set to not authenticated, and then try to auth it
		conn.setAuthenticated(false);
		
		//authenticate the user and store the success
		try {
			conn.setAuthenticated(Connection.getConn().authenticate(API_KEY, username, password));
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return Connection.isAuthenticated();
	}//end doInBackground

	/* Executes after the Login thread has completed */
	@Override
	protected abstract void onPostExecute(Boolean result);//end onPostExecute
}//end LoginUser
