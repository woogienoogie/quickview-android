
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

/**
 * @author Brendan White
 */
package com.ctctlabs.quickview;


import com.ctctlabs.quickview.webservice.CTCTConnection;

public class Connection {
	
	
	private static final CTCTConnection conn = new CTCTConnection();
	private static boolean isAuthenticated = false;

	/**  Get Connection Object     */
	public static CTCTConnection getConn() {
		return conn;
	}//end getConn

	/**   */
	public static boolean isAuthenticated() {
		return isAuthenticated;
	}//end isAuthenticated

	/**	  */
	public void setAuthenticated(boolean isAuthenticated) {
		Connection.isAuthenticated = isAuthenticated;
	}//end setAuthenticated

}//end Connection

