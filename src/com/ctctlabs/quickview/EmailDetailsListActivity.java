
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

/**
 * @author Brendan White
 */

package com.ctctlabs.quickview;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.auth.InvalidCredentialsException;
import org.apache.http.client.ClientProtocolException;

import com.ctctlabs.quickview.webservice.CTCTConnection;
import com.ctctlabs.quickview.webservice.CampaignEventIterator;
import com.ctctlabs.quickview.webservice.ModelObject;
import com.ctctlabs.quickview.webservice.CTCTConnection.EventType;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import android.widget.AdapterView.OnItemClickListener;

/** Activity to show a listing of email addresses that performed a certain action 
 *  on a particular campaign. For example address that opened the campaign,
 *  clicked the campaign, were bounced, etc... **/
public class EmailDetailsListActivity extends Activity
									  implements OnItemClickListener,
									  			 OnClickListener {
	
	private final int CAMPAIGNS_PER_PAGE = 50;
	private final int FIRST_PAGE = 1;
	
	//intent for showing contact details when a contact is selected
	static final String ACTION_SHOW_CONTACT_DETAILS = 
	     		 "com.ctctlabs.quickview.EmailDetailsListActivity.SHOW_CONTACT_DETAILS";
	
	//intent extras
	static final String EXTRA_CONTACT_ID = 
				 "com.ctctlabs.quickview.EmailDetailsListActivity.extra.CONTACT_ID";
	
	//passed intent variables
	private String campaignLink;
	private String eventHeading;
	private int eventType;
	
	//listview to display the contacts and the loading view
	private ListView mListView;
	private View vLoading;
	
	//main switcher and switcher for loading more contacts
	private ViewSwitcher mSwitcher;
	private ViewSwitcher mFooterSwitcher;
	
	//task to retrieve contacts
	private GetContactsTask mTask;
	private GetContactsTask mLoadMoreTask;
	
	//contacts that are displayed
	private ArrayList<HashMap<String, String>> contacts;
	CampaignEventIterator eventIter;
	
	//the last page(number) loaded and the current selection
	private int page;
	private int selection;
	
	/* Class to hold configuration data to be passed on orientation change */
	private class ContactsConfiguration extends Object {
		
		//the campaigns that have been loaded, and the iterator
		private ArrayList<HashMap<String, String>> contacts;
		private CampaignEventIterator eventIter;
		
		//the last page(number) loaded and the current selection
		private int page;
		private int selection;
		
		private boolean wasRunning; //was the load more task running when
									//the orientation changed?
		
		//constructor #1
		ContactsConfiguration(ArrayList<HashMap<String, String>> _contacts,
				CampaignEventIterator _eventIter, int _page,
							  boolean _wasRunning, int _selection) {
			this.contacts = _contacts; 	this.eventIter = _eventIter;
			this.page = _page; 			this.wasRunning = _wasRunning;
			this.selection = _selection;
		}//end constructor
	}//end CampaignConfiguration
	
	private void setupViews() {
    	//inflate the loading view
		vLoading = View.inflate(this, 
    			R.layout.loading, 
    			null);
    	
    	TextView tvHeading = (TextView)vLoading.findViewById(R.id.tv_heading_loading);
    	tvHeading.setVisibility(View.VISIBLE);
    	
    	tvHeading.setText(eventHeading);

    	//set the list heading view
    	TextView listHeading = (TextView)View.inflate(this, 
    						    R.layout.heading, 
    						    null);
    	listHeading.setVisibility(View.VISIBLE);
    	listHeading.setText(eventHeading);
    	
    	//create the list view to hold the contacts
    	mListView = new ListView(this);
    	mListView.addHeaderView(listHeading);
    	mListView.setOnItemClickListener(this);
		mListView.setBackgroundColor(Color.rgb(222, 222, 222));
		mListView.setCacheColorHint(0x00000000);
    	
    	//create the switchers
    	mSwitcher = new ViewSwitcher(this);
    	mFooterSwitcher = new ViewSwitcher(this);
    	
		//set the loading button
		Button loadButton = (Button)View.inflate(this, 
				R.layout.loadmore_button, 
				null);
		loadButton.setText(getString(R.string.str_loadmorecontacts));
		loadButton.setOnClickListener(this);
		
		//add the views to the footer
		mFooterSwitcher.addView(loadButton);
		mFooterSwitcher.addView(View.inflate(this, 
				R.layout.loading_footer, 
				null));
    }//end setupViews
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		//no title
		requestWindowFeature(Window.FEATURE_NO_TITLE);

        //get the intent and the intent variables
		Bundle extras = getIntent().getExtras();
		campaignLink = extras.getString("CAMPAIGN_LINK");
		eventType = extras.getInt("EVENT_TYPE");
		eventHeading = extras.getString("EVENT_HEADING");
        
        //setup the views
		setupViews();
		
		ContactsConfiguration passedConfig = 
			(ContactsConfiguration)getLastNonConfigurationInstance();
		if(passedConfig == null) {
			mSwitcher.addView(vLoading);
			mTask = new GetContactsTask();
			mTask.execute();
		} else {
			contacts = passedConfig.contacts;
			eventIter = passedConfig.eventIter;
			page = passedConfig.page;
			selection = passedConfig.selection;
			
			if(eventIter.hasNextPage()) {
				mListView.addFooterView(mFooterSwitcher);
			}//end if
			
			//show the contacts
			showContacts();
			
			if(passedConfig.wasRunning) {
				//move the selection and show the loading view
				mListView.setSelection(page * CAMPAIGNS_PER_PAGE);
				mFooterSwitcher.showNext();
				
				//start a new task
				mLoadMoreTask = new GetContactsTask();
				mLoadMoreTask.execute();
			}
		}//end else
		
		mSwitcher.addView(mListView);
		setContentView(mSwitcher);
    }//end onCreate
    
	/* Pass the contacts when the orientation changes */
    @Override
	public Object onRetainNonConfigurationInstance() {
    	ContactsConfiguration config;
    	
    	//if the main task was running when orientation changed return null
		//so the activity is restarted and cancel the current thread
		if(mTask != null && mTask.getStatus() == AsyncTask.Status.RUNNING) {
			mTask.cancel(true);
			return null;
		}//end if
		
		//if the load more task was running, let the new activity know through
		//"wasRunning"
		boolean wasRunning = false;
		if(mLoadMoreTask != null && mLoadMoreTask.getStatus() == 
								    AsyncTask.Status.RUNNING) {
			mLoadMoreTask.cancel(true);
			wasRunning = true;
		}//end if
		
		//get the configuration and pass it
		config = new ContactsConfiguration(contacts, eventIter, 
										   page, wasRunning, selection);
		
    	return config;
	}//end onRetainNonConfigurationInstance

	/* GetCampaignTask retrieves a single campaign in the background */
	private class GetContactsTask extends AsyncTask<Void, Integer, 
												ArrayList<HashMap<String, String>>> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if(contacts == null)
				contacts = new ArrayList<HashMap<String, String>>();
		}//end onPreExecute

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(Void... arg0) {
			//the event type to get contacts for
			CTCTConnection.EventType type = null;
			
			//determine event type
			switch(eventType) {
				case 0:
					type = EventType.OPENS;
					break;
				case 1:
					type = EventType.CLICKS;
					break;
				case 2:
					type = EventType.FORWARDS;
					break;
				case 3:
					type = EventType.OPTOUTS;
					break;
				case 4:
					break;
				case 5:
					type = EventType.BOUNCES;
					break;
			}//end switch
			
			//HANDLE CLICKS, WILL MOVE IN BETA2
			if(type == EventType.CLICKS) {
				ArrayList<CampaignEventIterator> iterList;
				ArrayList<ModelObject> clickEvents;
				HashMap<String, String> clickContact;
				try {
					iterList = Connection.getConn().getCampaignEvents(campaignLink, type);
					CampaignEventIterator iter;
					for(int i = 0; i < iterList.size(); i++) {
						iter = iterList.get(i);
						while(iter.hasNextPage()) {
							iter.loadNextPage();
						}//end while
						clickEvents = iter.getLoadedEntries();
						for(int j = 0; j < clickEvents.size(); j++) {
							clickContact = new HashMap<String, String>();
							String emailAddress = clickEvents.get(j).getAttribute("EmailAddress").toString();
							
							boolean isInClicksList = false;
							for(int k = 0; k < contacts.size(); k++) {
								if((!contacts.isEmpty() &&
										contacts.get(k).containsValue(emailAddress))) {
									isInClicksList = true;
									break;
								}//end if
							}//end for with k
							if(!isInClicksList || contacts.isEmpty()) {
								clickContact.put("EMAIL", emailAddress);
								clickContact.put("LINK", clickEvents.get(j)
														  .getAttribute("ContactLink")
														  .toString());
								contacts.add(clickContact);
							}//end if
						}//end for with j
					}//end for
				} catch (InvalidCredentialsException e) {
					e.printStackTrace();
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return contacts;
			}
			
			//campaign events
			ArrayList<ModelObject> events;
			HashMap<String, String> contact = null;
			
			try {
				if(eventIter == null)
					eventIter = Connection.getConn()
										  .getCampaignEvents(campaignLink, type)
										  .get(0);
				else
					eventIter.loadNextPage();
				
				events = eventIter.getLoadedEntries();
				int i = selection = page * CAMPAIGNS_PER_PAGE;
				for(; i < events.size(); i++) {
					contact = new HashMap<String, String>();
					String emailAddress = events.get(i)
												.getAttribute("EmailAddress")
												.toString();
					
					boolean isInList = false;
					for(int j = 0; j < contacts.size(); j++) {
						if((!contacts.isEmpty()) && 
								contacts.get(j).containsValue(emailAddress)) {
							isInList = true;
							break;
						}//end if
					}//end inner for
					if(!isInList || contacts.isEmpty()) {
						contact.put("EMAIL", emailAddress);
						contact.put("LINK", events.get(i)
												  .getAttribute("ContactLink")
												  .toString());
						contacts.add(contact);
					}//end if
				}//end outer for
			} catch (InvalidCredentialsException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			page++;
			return contacts;
		}//end doInBackground

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
			super.onPostExecute(result);
			//ONLY UNTIL BETA 2, CLICKS WILL NOT BE HANDLED IN THIS CLASS > beta1
			if(eventType == 1) {
				showContacts();
				mSwitcher.showNext();
				return;
			}
			if(page == FIRST_PAGE) {
				if(eventIter.hasNextPage()) {
					mListView.addFooterView(mFooterSwitcher);
				} 
				showContacts();
				mSwitcher.showNext();
			} else {
				if(!eventIter.hasNextPage()) {
					mListView.removeFooterView(mFooterSwitcher);
				} else {
					mFooterSwitcher.showPrevious();
				}
				showContacts();
			}//end else
			mListView.setSelection(selection);
		}//end onPostExecute
	}//end GetContactsTask
	
	/* Show the contacts that were retrieved */
	private void showContacts() {
		mListView.setAdapter(new ContactsAdapter(this, contacts));
	}//end showContacts

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, 
							int position, long positionAsLong) {
		//adjust for added headerview
		position--;
		
		Intent intent = new Intent(ACTION_SHOW_CONTACT_DETAILS);
		intent.putExtra(EXTRA_CONTACT_ID, contacts.get(position).get("LINK"));
		startActivity(intent);
	}//end onItemClick

	@Override
	public void onClick(View view) {
		switch(view.getId()) {
			case R.id.btn_loadmorecontacts:
				mFooterSwitcher.showNext();
				mLoadMoreTask = new GetContactsTask(); 
				mLoadMoreTask.execute();
				break;
		}//end switch
	}//end onclick
}//end EmailDetailsListActivity
