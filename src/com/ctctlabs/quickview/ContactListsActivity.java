
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

/**
 * @author Brendan White
 */

package com.ctctlabs.quickview;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.http.auth.InvalidCredentialsException;
import org.apache.http.client.ClientProtocolException;

import com.ctctlabs.quickview.webservice.ContactListIterator;
import com.ctctlabs.quickview.webservice.ModelObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.ViewSwitcher;
import android.widget.AdapterView.OnItemClickListener;

/** Activity to display ALL site owners contact lists. Lists are loaded 50 at
 *  a time. From the Menu there are the options to logout and to add a new list **/
public class ContactListsActivity extends Activity 
								  implements OnClickListener, 
								  			 OnItemClickListener {

	private final int LISTS_PER_PAGE = 50;
	private final int FIRST_PAGE = 1;
	
	//menu items for logging out and adding a new list
	private final int MENU_LOGOUT = 0;
	private final int MENU_ADD_LIST = 1;
	
	//requests for adding and modifying a list
	static final int ADD_LIST_REQUEST = 0;
	static final int MODIFY_LIST_REQUEST = 1;
	
	//intent variables --> ListDetailsActivity
	static final String ACTION_SHOW_LIST_MEMBERS = 
			     "com.ctctlabs.quickview.ContactListsActivity.SHOW_LIST_MEMBERS";
	static final String EXTRA_LISTNAME = 
				"com.ctctlabs.quickview.ContactListsActivity.extra.LISTNAME";
	static final String EXTRA_LISTLINK = 
				"com.ctctlabs.quickview.ContactListsActivity.extra.LISTLINK";
	
	//activity layouts and views
	private ListView mListView;
	
	//switcher for main view and for footer view
	private ViewSwitcher mSwitcher;
	private ViewSwitcher mFooterSwitcher;
	
	//loaded contact lists
	private ArrayList<HashMap<String, String>> lists;
	
	//tasks for retrieving lists and additional lists
	private GetListsTask mTask;
	private GetListsTask mLoadMoreTask;
	
	//iterator for loaded lists
	private ContactListIterator listIter;
	
	//last page accessed and currently selected list
	private int page = 0;
	private int selection = 0;
	
	/* Class to hold configuration data to be passed on orientation change */
	private class ListsConfiguration extends Object {
		
		//the lists that have been loaded, and the iterator
		private ArrayList<HashMap<String, String>> lists;
		private ContactListIterator listIter;
		
		//the last page(number) loaded and the current selection
		private int page;
		private int selection;
		
		private boolean wasRunning; //was the load more task running when
									//the orientation changed?
		
		//constructor #1
		ListsConfiguration(ArrayList<HashMap<String, String>> _lists,
							  ContactListIterator _listIter, int _page,
							  boolean _wasRunning, int _selection) {
			this.lists = _lists; 	this.listIter = _listIter;
			this.page = _page; 		this.wasRunning = _wasRunning;
			this.selection = _selection;
		}//end constructor
	}//end ListsConfiguration
	
	private void setupViews() {
		//setup the list view
		mListView = new ListView(this);
		mListView.setOnItemClickListener(this);
		mListView.setBackgroundColor(Color.rgb(222, 222, 222));
		mListView.setCacheColorHint(0x00000000);
		
		//and the switcher
		mSwitcher = new ViewSwitcher(this);
		
		//and the footer view
		mFooterSwitcher = new ViewSwitcher(this);
		
		Button loadButton = (Button)View.inflate(this, R.layout.loadmore_button, null);
		loadButton.setText("Load more lists...");
		loadButton.setOnClickListener(this);
		
		mFooterSwitcher.addView(loadButton);
		mFooterSwitcher.addView(View.inflate(this, R.layout.loading_footer, null));
	}//end setupViews
	
	/** Called when the activity is first created. */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//no title
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		//setup views
		setupViews();
		
		//holds contact list data to be passed between orientation changes
		ListsConfiguration passedConfig = 
			(ListsConfiguration) getLastNonConfigurationInstance();
		
		//if no passed data, go get lists
		if(passedConfig == null) {
			mSwitcher.addView(View.inflate(this, 
					R.layout.loading, 
					null));
			mTask = new GetListsTask(); 
			mTask.execute(); //get the lists in the background
		} else {
			lists = passedConfig.lists;
			listIter = passedConfig.listIter;
			
			page = passedConfig.page;
			selection = passedConfig.selection;
			
			//if there is a next page add the footer
			if(listIter.hasNextPage()) {
				mListView.addFooterView(mFooterSwitcher);
			}//end if
			
			//show the campaigns
			showLists();
			
			//the load more campaigns task was running when the orientation
			if(passedConfig.wasRunning) {
				//move the selection and show the loading view
				mListView.setSelection(page * LISTS_PER_PAGE);
				mFooterSwitcher.showNext();
				
				//start a new task
				mLoadMoreTask = new GetListsTask();
				mLoadMoreTask.execute();
			}
		}//end else
		
		mSwitcher.addView(mListView);
		setContentView(mSwitcher);
	}//end onCreate
	
	@Override
	public Object onRetainNonConfigurationInstance() {
		ListsConfiguration config;
		
		//if the main task was running when orientation changed return null
		//so the activity is restarted and cancel the current thread
		if(mTask != null && mTask.getStatus() == AsyncTask.Status.RUNNING) {
			mTask.cancel(true);
			return null;
		}//end if
		
		//if the load more task was running, let the new activity know through
		//"wasRunning"
		boolean wasRunning = false;
		if(mLoadMoreTask != null && mLoadMoreTask.getStatus() == 
								    AsyncTask.Status.RUNNING) {
			mLoadMoreTask.cancel(true);
			wasRunning = true;
		}//end if
		
		//get the configuration and pass it
		config = new ListsConfiguration(lists, listIter, 
										   page, wasRunning, selection);	
		return config;
	}//end onRetainNonConfigurationInstance
	
	/*@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		
		if(retrievedLists != null && 
				retrievedLists.getStatus() == AsyncTask.Status.RUNNING) {
			try {
				contactLists = retrievedLists.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
	}//end onConfigurationChanged*/

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, 
							int position, long positionAsLong) {
		//get the selected list
		HashMap<String, String> selectedList = lists.get(position);
		
		//setup the intent
		Intent intent = new Intent(ACTION_SHOW_LIST_MEMBERS);
		intent.putExtra(EXTRA_LISTNAME, selectedList.get("NAME"));
		intent.putExtra(EXTRA_LISTLINK, selectedList.get("LINK"));
		
		//start the activity
		startActivityForResult(intent, MODIFY_LIST_REQUEST);
	}//end onItemClick

	@Override
	public void onClick(View view) {
		switch(view.getId()) {
		case R.id.btn_loadmorecontacts:
			mFooterSwitcher.showNext();
			mLoadMoreTask = new GetListsTask(); 
			mLoadMoreTask.execute();
			break;
		}//end switch
	}//end onClick
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, MENU_LOGOUT, 0, "Log out")
			.setIcon(R.drawable.logout_icon);
		menu.add(0, MENU_ADD_LIST, 0, "Add list")
		.setIcon(android.R.drawable.ic_menu_add);
        return true;
	}//end onCreateOptionsMenu
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
    		case MENU_LOGOUT:
    			QuickViewHelper.showLogOutDialog(this, this);
    			return true;
			case MENU_ADD_LIST:
				startActivityForResult(new Intent(this, AddListActivity.class), ADD_LIST_REQUEST);
        		return true;
        }//end switch
            
		return false;
	}//end onOptionsItemSelected
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode) {
			case ADD_LIST_REQUEST:
				if(resultCode == Activity.RESULT_OK) {
					HashMap<String, String> createdList = new HashMap<String, String>();
					createdList.put("NAME", data.getStringExtra("LIST_NAME"));
					createdList.put("LINK", data.getStringExtra("LIST_LINK"));
					lists.add(0, createdList);
				}//end if
				break;
			case MODIFY_LIST_REQUEST:
				if(resultCode == Activity.RESULT_OK) {
					String listToDelete = data.getStringExtra("LIST_LINK");
					for(int i = 0; i < lists.size(); i++)
						if(lists.get(i).get("LINK").equals(listToDelete))
							lists.remove(i);
				}//end if
				break;	
		}//end switch
		showLists();
	}//end onActivityResult
	
	private class GetListsTask extends AsyncTask<Void, Integer, 
													  ArrayList<HashMap<String, String>>> {
		@Override
		protected void onPreExecute() {
			if(lists == null)
				lists = new ArrayList<HashMap<String, String>>();
		}//end onPreExecute

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(Void... isLoadNext) {
			
			//individual list and the collection of loaded lists
			HashMap<String, String> list = null;
			ArrayList<ModelObject> loadedLists;
			
			try {
				//if it is the first
				if(listIter == null)
					listIter = Connection.getConn().getContactLists();
				else
					listIter.loadNextPage();
					
				//get the loaded lists
				loadedLists = listIter.getLoadedEntries();
				
				int i = selection = page * LISTS_PER_PAGE;
				for(; i < loadedLists.size(); i++)
				{
					list = new HashMap<String, String>();
					list.put("NAME", loadedLists.get(i).getAttribute("Name").toString());
					list.put("LINK", loadedLists.get(i).getAttribute("Link").toString());
					
					if(!(list.containsValue("Active") || 
							list.containsValue("Do Not Mail") || 
								list.containsValue("Removed"))) {
						lists.add(list);
					} else {
						selection --;
					}
				}//end for
					
			} catch (InvalidCredentialsException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			page++;
			return lists;
		}//end doInBackground

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
			super.onPostExecute(result);
			if(lists.isEmpty()) {
				showEmptyListView();
				return;
			}//end if
			
			if(page == FIRST_PAGE) {
				if(listIter.hasNextPage()) {
					mListView.addFooterView(mFooterSwitcher);
				} 
				showLists();
				mSwitcher.showNext();
			} else {
				if(!listIter.hasNextPage()) {
					mListView.removeFooterView(mFooterSwitcher);
				} else {
					mFooterSwitcher.showPrevious();
				}
				showLists();
			}
			mListView.setSelection(selection);
		}//end onPostExecute
	}//end RetrieveListsTask
	
	/* Show the retrieved lists */
	private void showLists() {
		mListView.setAdapter(new SimpleAdapter(this, lists, R.layout.simple_list_item_1_blk,
				new String[] { "NAME" }, new int[] { android.R.id.text1 }));
	}//end setListAdapter
	
	private void showEmptyListView() {
		View noListsView = View.inflate(this, R.layout.contactlists_nodata, null);
		setContentView(noListsView);
	}//end showEmptyListView
}//end ContactListsActivity
