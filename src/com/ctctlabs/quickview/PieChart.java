
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

/**
 * @author Brendan White
 */

package com.ctctlabs.quickview;

import android.util.Log;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Paint.Join;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;

	public class PieChart extends Drawable {

		private RectF arcBounds;
		private BitmapDrawable backgroundBitmap;
		
		private Paint paint;
		
		private float openPercentage;
		private float clickPercentage;
		
		@SuppressWarnings("unused")
		public PieChart(RectF _arcBounds, BitmapDrawable _backgroundBitmap, float _openPercentage, float _clickPercentage) { 
			
			arcBounds = _arcBounds;
			backgroundBitmap = _backgroundBitmap;
			openPercentage = _openPercentage;
			clickPercentage = _clickPercentage;
			
			paint = new Paint();


		}//end PieChart constructor

		@Override
		public void draw(Canvas canvas) {
			
			// start drawing angles from 12 o'clock
			final float startingArcDegree = 270;
			
			// calculate angle sweeps
			float opensArcDegrees = 360f * openPercentage;
			float clicksArcDegrees = 360f * clickPercentage;
			
			// calculate outline bounds
			RectF outlineBounds = new RectF(arcBounds.left - 10, arcBounds.top - 10, arcBounds.right + 10, arcBounds.bottom + 10);
			
			// draw clicks fill
			paint.setColor(Color.BLACK);
			paint.setStyle(Paint.Style.FILL);			
			canvas.drawArc(outlineBounds, startingArcDegree, clicksArcDegrees, true, paint);
			
			// draw clicks outline
			paint.setColor(R.color.piechart_clickcolor);
			paint.setAntiAlias(true);
			paint.setStyle(Paint.Style.STROKE);
			paint.setStrokeWidth(1);
			canvas.drawArc(outlineBounds, startingArcDegree, opensArcDegrees, true, paint);

			// draw background image
			canvas.drawBitmap(Bitmap.createScaledBitmap(backgroundBitmap.getBitmap(),(int)(arcBounds.left + arcBounds.right), (int)(arcBounds.top + arcBounds.bottom), true), 0, 0, paint);
			
			// draw opens
			paint.setColor(R.color.piechart_opencolor);
			paint.setStyle(Paint.Style.FILL);
			paint.setAlpha(100); 
			canvas.drawArc(arcBounds, startingArcDegree, opensArcDegrees, true, paint);

			
			/*
			int slice = 0, i = 0;
			float total = 0;
			
			for (float data : chartData)
				total += data;
			
			float currentArcDegrees = 270;
			for (float data : chartData) 
			{
				Log.i("data: " + data + )
				float arcSweep = total == 0 ? 0 : 360 * data / (float) total;
				
				float nextArcDegrees =  currentArcDegrees + arcSweep;
				
				int color = chartColors[i % chartColors.length];
				paint.setColor(color);
				paint.setAntiAlias(true);
				paint.setStyle(Paint.Style.FILL);
				
				if(hasOpensBorder && slice == 0) {
				paint.setAlpha(0x4F); 
				}
				
				// *** draw opens
				canvas.drawArc(arcBounds, currentArcDegrees, arcSweep, true, paint);
		
				paint.setStyle(Paint.Style.STROKE);
				paint.setStrokeJoin(Join.ROUND);
				
				if(clicksBorderLen > 0 && slice == 1) {
					paint.setStrokeWidth(1);
					paint.setColor(Color.parseColor("#888888"));
					arcSweep = clicksBorderLen;
				}
				else if(hasOpensBorder && slice == 0) {
						//paint.setStrokeWidth(1);
						//paint.setColor(Color.BLACK);
				}
				
				// draw clicks
				canvas.drawArc(arcBounds, currentArcDegrees, arcSweep, true, paint);

				currentArcDegrees = nextArcDegrees;
				i++;
				slice = i;
			}//end for
			*/
			
		}//end draw

		@Override
		public int getOpacity() {
			return paint.getAlpha();
		}//end getOpacity

		@Override
		public void setAlpha(int alpha) {
			paint.setAlpha(alpha);
		}//end setAlpha

		@Override
		public void setColorFilter(ColorFilter cf) {
			paint.setColorFilter(cf);
		}//end setColorFilter
	}//end PieChart
