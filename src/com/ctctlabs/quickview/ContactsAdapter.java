
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

/**
 * @author Brendan White
 */

package com.ctctlabs.quickview;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/** ContactsAdapter **/
public class ContactsAdapter extends BaseAdapter {

	private ArrayList<HashMap<String, String>> contacts;
	private Context context;
	
	@SuppressWarnings("unchecked")
	public ContactsAdapter(Context _context, ArrayList _contacts) {
		super();
		this.contacts = _contacts;
		this.context = _context;
	}//end ContactsAdapter constructor

	public View getView(int position, View convertView, ViewGroup parent) {
		View mView = convertView;
		
		if(mView == null)
			mView = View.inflate(context, R.layout.contacts_row, null);
		
		HashMap<String, String> contact = contacts.get(position);
		if(contact != null) {
			String emailAddress = contact.get("EMAIL");
			String[] splits = emailAddress.split("@");
			
			TextView left = (TextView)mView.findViewById(R.id.row_text1);
			TextView right = (TextView)mView.findViewById(R.id.row_text2);
			
			if(splits.length == 2) {
				left.setText(splits[0]);
				right.setText("@" + splits[1]);
			}	
		}//end outer if
		
		return mView;
	}//end getView

	public int getCount() {
		return contacts.size();
	}//end getCount

	public Object getItem(int position) {
		return contacts.get(position);
	}//end getItem

	public long getItemId(int position) {
		return position;
	}//end getItemId
}//end ContactsAdapter
