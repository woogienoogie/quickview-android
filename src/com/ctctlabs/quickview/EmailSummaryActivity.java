
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

/**
 * @author Brendan White
 */

package com.ctctlabs.quickview;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.http.auth.InvalidCredentialsException;
import org.apache.http.client.ClientProtocolException;

import com.ctctlabs.quickview.webservice.CampaignIterator;
import com.ctctlabs.quickview.webservice.ModelObject;
import com.ctctlabs.quickview.webservice.CTCTConnection.CampaignType;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import android.widget.AdapterView.OnItemClickListener;

/** Activity to list a user's sent campaigns **/
public class EmailSummaryActivity extends Activity 
								  implements OnClickListener, 
								  			 OnItemClickListener {
	
	//email summary menu items
	private final int MENU_LOGOUT = 0;
	private final int MENU_REFRESH = 1;
	
	private final int FIRST_CAMPAIGN = 0;
	private final int FIRST_PAGE = 1;
	
	//number of campaigns returned per page
	private final int CAMPAIGNS_PER_PAGE = 50;
	
	//intent to be passed to EmailDetailsActivity
	static final String ACTION_SHOW_EMAIL_DETAILS = 
		         "com.ctctlabs.quickview.EmailSummaryActivity.SHOW_EMAIL_DETAILS";
	
	//intent extras for selected campaign
	static final String EXTRA_LINK = 
				 "com.ctctlabs.quickview.EmailSummaryActivity.extra.LINK";
	
	//campaigns and campaign iterator
	private ArrayList<HashMap<String,String>> campaigns;
	private CampaignIterator campaignIter;
	
	//the last accessed page and the current selection
	private int page = 0;
	private int selection = 0;
	
	//task to retrieve campaigns in the background
	private GetCampaignsTask mTask;
	private GetCampaignsTask mLoadMoreTask;
	
	//listview to display campaigns
	private ListView mListView;
	
	//switches between loading view and listview
	private ViewSwitcher mSwitcher;
	private ViewSwitcher mFooterSwitcher; //switcher for footer
	
	/* Class to hold configuration data to be passed on orientation change */
	private class CampaignConfiguration extends Object {
		
		//the campaigns that have been loaded, and the iterator
		private ArrayList<HashMap<String, String>> campaigns;
		private CampaignIterator campaignIter;
		
		//the last page(number) loaded and the current selection
		private int page;
		private int selection;
		
		private boolean wasRunning; //was the load more task running when
									//the orientation changed?
		
		//constructor #1
		CampaignConfiguration(ArrayList<HashMap<String, String>> _campaigns,
							  CampaignIterator _campaignIter, int _page,
							  boolean _wasRunning, int _selection) {
			campaigns = _campaigns; campaignIter = _campaignIter;
			page = _page; 			wasRunning = _wasRunning;
			selection = _selection;
		}//end constructor
	}//end CampaignConfiguration
	
	/* Pass the loaded campaigns between orientations */
	@Override
	public Object onRetainNonConfigurationInstance() {
		CampaignConfiguration config;
		
		//if the main task was running when orientation changed return null
		//so the activity is restarted and cancel the current thread
		if(mTask != null && mTask.getStatus() == AsyncTask.Status.RUNNING) {
			mTask.cancel(true);
			return null;
		}//end if
		
		//if the load more task was running, let the new activity know through
		//"wasRunning"
		boolean wasRunning = false;
		if(mLoadMoreTask != null && mLoadMoreTask.getStatus() == 
								    AsyncTask.Status.RUNNING) {
			mLoadMoreTask.cancel(true);
			wasRunning = true;
		}//end if
		
		//get the configuration and pass it
		config = new CampaignConfiguration(campaigns, campaignIter, 
										   page, wasRunning, selection);	
		return config;
	}//end onRetainNonConfigurationInstance

	/* Setup the views for the activity */
	private void setupViews() {
		//create the ListView
		mListView = new ListView(this);
		mListView.setOnItemClickListener(this);
		mListView.setEmptyView(findViewById(R.id.btn_bounces));
		mListView.setBackgroundColor(Color.rgb(215, 215, 215));
		mListView.setCacheColorHint(0x00000000);
		
		//create the ViewSwitcher for the activity
		mSwitcher = new ViewSwitcher(this);
		
		//create the new switcher view
		mFooterSwitcher = new ViewSwitcher(this);
		
		//set the loading button
		Button loadButton = (Button)View.inflate(this, 
				R.layout.loadmore_button, 
				null);
		loadButton.setText(getString(R.string.str_loadmorecampaigns));
		loadButton.setOnClickListener(this);
		
		//add the views
		mFooterSwitcher.addView(loadButton);
		mFooterSwitcher.addView(View.inflate(this, 
				R.layout.loading_footer, 
				null));
	}//end setupViews
	
	/* Called when the activity is first created. */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//setup the views
		setupViews();
		
		//if no data is passed then start a new task to get the campaigns
		//else set the passed campaigns and show the campaigns
		CampaignConfiguration passedConfig = 
			(CampaignConfiguration)getLastNonConfigurationInstance();
		
		if(passedConfig == null) {
			mSwitcher.addView(View.inflate(this, 
					R.layout.loading, 
					null));
			mTask = new GetCampaignsTask(); 
			mTask.execute();
		}//end if
		else {
			
			//update campaign data
			campaigns = passedConfig.campaigns;
			
			if(campaigns.isEmpty()) {
				showEmptyView();
				return;
			}//end if
			
			//update campaign iterator
			campaignIter = passedConfig.campaignIter;
			
			//update page and selection data
			selection = passedConfig.selection;
			page = passedConfig.page;
			
			//if there is a next page add the footer
			if(campaignIter.hasNextPage()) {
				mListView.addFooterView(mFooterSwitcher);
			}
			
			//show the campaigns
			showCampaigns();
			
			//the load more campaigns task was running when the orientation
			if(passedConfig.wasRunning) {
				//move the selection and show the loading view
				mListView.setSelection(page * CAMPAIGNS_PER_PAGE);
				mFooterSwitcher.showNext();
				
				//start a new task
				mLoadMoreTask = new GetCampaignsTask();
				mLoadMoreTask.execute();
			}
		}//end else
		
		//add the ListView
		mSwitcher.addView(mListView);
		setContentView(mSwitcher);
	}//end onCreate

	/* Get the most recent campaign view */
	private View getMostRecent(HashMap<String, String> firstCampaign) {
		int sent = 0, clicks = 0, opens = 0, bounces = 0; 
		float clickRate = 0, openRate = 0;
		
		//format for displaying percentage
		DecimalFormat formatter = new DecimalFormat("#0");
		
		//from the first campaign, get sent, clicks, and opens
		bounces = Integer.parseInt(firstCampaign.get("BOUNCES"));
		sent = Integer.parseInt(firstCampaign.get("SENT")) - bounces;
		clicks = Integer.parseInt(firstCampaign.get("CLICKS"));
		opens = Integer.parseInt(firstCampaign.get("OPENS"));
		
		//calculate the openrate
		openRate = calculateOpenRate(opens, sent);
		clickRate = calculateClickRate(clicks, opens);
		
		//inflate the EmailSummary View
		View mRecentView = View.inflate(this, R.layout.email_summary_view, null);
		
		//to support 1.5 you can't reference and onClick method in the layout so
		//seting the linear layouts onClick property in the code is requried
		LinearLayout recentLayout = 
			(LinearLayout)mRecentView.findViewById(R.id.layout_mostRecentClickable);
		recentLayout.setOnClickListener(this);

		//set the name for the most recent campaign
		TextView tvName = (TextView)mRecentView.findViewById(R.id.tv_campaign_name);
		tvName.setText(firstCampaign.get("NAME").toString());
		
		//set the date for the most recent campaign
		TextView tvDate = (TextView)mRecentView.findViewById(R.id.tv_campaign_date);
		tvDate.setText(firstCampaign.get("DATE").toString());
		
		//set the open rate for the most recent campaign
		TextView tvOpenRate = (TextView)mRecentView.findViewById(R.id.tv_openrate);
		tvOpenRate.setText(formatter.format(openRate) + "% open rate");
		
		//set the click rate for the most recent campaign
		TextView tvClickRate = (TextView)mRecentView.findViewById(R.id.tv_clickrate);
		tvClickRate.setText(formatter.format(clickRate) + "% click rate");
		
        //graph for the most recent campaign
		ImageView ivGraph = (ImageView)mRecentView.findViewById(R.id.pie_graph);
		
		//if there are no opens, show an empty graph
		if(opens == 0)
			ivGraph.setImageResource(R.drawable.none);
		//otherwise create the graph image using the most recent data
		else
		{
			BitmapDrawable bg = (BitmapDrawable)getResources().getDrawable(R.drawable.background);
	        Drawable layer = createPieChart(11f, 12f, 137, 137, bg, (openRate / 100f), (clickRate / 100f));
			ivGraph.setImageDrawable(layer);

		}
		return mRecentView;
	}//end getMostRecentCampaignView

	/* Given the opens and sent emails for a campaign, calculate the openrate */
	protected static float calculateOpenRate(int opens, int sent) {
		if(opens != 0)
			return (opens / (float)sent)*100;
		return 0;
	}//end getOpenRate
	
	/* Given the clicks and opens for a campaign, calculate the clickrate */
	protected static float calculateClickRate(int clicks, int opens) {
		if(clicks != 0)
			return (clicks / (float)opens)*100;
		return 0;
	}//end getClickRate
	
	/* Create a Pie Chart graphic with the requested parameters */

	private PieChart createPieChart(float left, float top,
									float right, float bottom,
									BitmapDrawable backgroundBitmap,
									float openPercentage, float clickPercentage) {
		RectF bounds = new RectF(left, top, right, bottom);
		PieChart chart = new PieChart(bounds, backgroundBitmap, openPercentage, clickPercentage);
		return chart;
	}//end createPieChart

	/* Convert CTCT timestamp to a date in the form "Jan 1, 2010" */
	private String convertDate(String date) {
		String convertedDate = "";
		
		//from and to formats
		SimpleDateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat toFormat = new SimpleDateFormat("MMM dd yyyy");
		
		try {
			//convert the date
			convertedDate = toFormat.format(fromFormat.parse(date));
		} catch (ParseException e) {
			//there is a parsing problem, don't bother including the date
		}
		return convertedDate;
	}//end convertDate
	
	/* Create a new intent */
	private Intent createIntent(HashMap<String, String> campaign) {
		Intent intent = new Intent(ACTION_SHOW_EMAIL_DETAILS);
		intent.putExtra(EXTRA_LINK, campaign.get("LINK").toString());
		
		//return created intent
		return intent;
	}//end Intent
	
	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, 
						    int position, long positionAsLong) {
		startActivity(createIntent(campaigns.get(position)));
	}//end onItemClick
	
	@Override
	public void onClick(View view) {
		switch(view.getId()) {
			case R.id.btn_loadmorecontacts:
				mFooterSwitcher.showNext();
				mLoadMoreTask = new GetCampaignsTask(); 
				mLoadMoreTask.execute();
				break;
			case R.id.layout_mostRecentClickable:
				startActivity(createIntent(campaigns.get(FIRST_CAMPAIGN)));
				break;
		}//end switch
	}//end onclick
	
	/* Create menu options */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, MENU_LOGOUT, 0, R.string.str_logoutmenu)
			.setIcon(R.drawable.logout_icon);
		menu.add(0, MENU_REFRESH, 0, R.string.str_refreshmenu)
			.setIcon(R.drawable.refresh_icon);
        return true;
	}//end onCreateOptionsMenu
	
	/* Called when a menu item has been selected. */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {  
		switch(item.getItemId()) {
    		case MENU_LOGOUT:
    			QuickViewHelper.showLogOutDialog(this, this);
    			return true;
		
			case MENU_REFRESH:
				finish();
				startActivity(new Intent(this, QuickViewActivity.class));
        		return true;   
        }//end switch       
		return false;
	}//end onOptionsItemSelected
	
	/* Custom Adapter to render retrieved Campaigns */
	private class CampaignAdapter extends BaseAdapter {

		//context 
		private Context context;
		
		//retrieved campaigns
		private ArrayList<HashMap<String, String>> campaigns;
		
		//most recent campaign view
		private View mostRecent;
		
		/* Constructor #1 */
		@SuppressWarnings({ "unchecked" })
		public CampaignAdapter(Context _context, ArrayList _campaigns, 
							   View _mostRecent) {
			this.context = _context;
			this.campaigns = _campaigns;
			this.mostRecent = _mostRecent;
		}//end constructor1
		
		/* Constructor #2 */
		@SuppressWarnings("unused")
		public CampaignAdapter(Context _context, ArrayList _campaigns) {
			this.context = _context;
			this.campaigns = _campaigns;
		}//end constructor2
		
		/* Constructor #3 */
		@SuppressWarnings("unused")
		public CampaignAdapter(Context _context) {
			this.context = _context;
		}//end constructor3
		
		/* Get the count of items */
		@Override
		public int getCount() {
			return campaigns.size();
		}

		/* Get a paticular item given its position */
		@Override
		public Object getItem(int position) {
			return campaigns.get(position);
		}

		/* Get the itemId, in this case it is just the position */
		@Override
		public long getItemId(int position) {
			return position;
		}

		/* Each list item is a view, getView creates those views */
		@Override
		public View getView(int position, View convertView, 
						 	ViewGroup parent) {
			//contains created view or null if list has not been created yet
			View mView = convertView;
			
			//if it's the first view item, return the most recent view
			if(position == 0)
				return mostRecent;

			//inflate the view, which is really a row of name and dates
			mView = View.inflate(context, R.layout.list_item_row, null);
					
			//get and set the campaign name and date
			HashMap<String, String> campaign = campaigns.get(position);
			if(campaign != null) {
				String name = campaign.get("NAME");
				String date = campaign.get("DATE");
					
				TextView nameTView = (TextView)mView
									  .findViewById(android.R.id.text1);
				nameTView.setText(name);
					
				TextView dateTView = (TextView)mView
									  .findViewById(android.R.id.text2);
				dateTView.setText(date);
			}		
			return mView;
		}//end getView
	}//end CampaignAdapter
	
	/* GetCampaignsTask retrieves campaigns in the background */
	private class GetCampaignsTask extends AsyncTask<Void, Integer, 
	  									   ArrayList<HashMap<String, String>>> {
		/* Called before task is executed; here create the new array list */
		@Override
		protected void onPreExecute() {
			if(campaigns == null)
				campaigns = new ArrayList<HashMap<String,String>>();
		}//end onPreExecute

		/* Retrieves campaigns in the background */
		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(
				  Void... arg0) {
			
			//campaign to add
			HashMap<String, String> campaign = null;
			
			//all the campaigns that have been loaded
			ArrayList<ModelObject> loadedCampaigns = null;
			
			try {
				
				if(campaignIter == null)
					campaignIter = Connection.getConn().getCampaigns(CampaignType.SENT);
				else
					campaignIter.loadNextPage();
				
				//get the entries that have been loaded
				loadedCampaigns = campaignIter.getLoadedEntries();
				
				//start getting campaigns where the iterator left off
				int i = selection = page * CAMPAIGNS_PER_PAGE;
				
				//for every loaded entry, get the relevant data
				for(; i < loadedCampaigns.size(); i++)
				{
					campaign = new HashMap<String, String>();
					
					campaign.put("LINK", 
							loadedCampaigns.get(i).getAttribute("Link").toString());
					campaign.put("NAME", 
							loadedCampaigns.get(i).getAttribute("Name").toString());
						
					String date = loadedCampaigns.get(i).getAttribute("Date").toString();
					campaign.put("DATE", convertDate(date));
					
					//if it is the first campaign, get additional data
					if(i == FIRST_CAMPAIGN) {
						campaign.put("SENT", 
								loadedCampaigns.get(i).getAttribute("Sent").toString());
						campaign.put("OPENS", 
								loadedCampaigns.get(i).getAttribute("Opens").toString());
						campaign.put("CLICKS", 
								loadedCampaigns.get(i).getAttribute("Clicks").toString());
						campaign.put("BOUNCES", 
								loadedCampaigns.get(i).getAttribute("Bounces").toString());
					}//end if
					
					//add the campaign
					campaigns.add(campaign);
				}//end for
				
			} catch (InvalidCredentialsException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} 
			page++;
			return campaigns;
		}

		/* Called when the task is canceled */
		@Override
		protected void onCancelled() {
			super.onCancelled();
		}//end onCancelled

		/* Called when the background task is finished, campaigns are returned */
		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
			if(campaigns.isEmpty()) {
				showEmptyView();
				return;
			}
			
			if(page == FIRST_PAGE) {
				if(campaignIter.hasNextPage()) {
					mListView.addFooterView(mFooterSwitcher);
				} 
				showCampaigns();
				mSwitcher.showNext();
			} else {
				if(!campaignIter.hasNextPage()) {
					mListView.removeFooterView(mFooterSwitcher);
				} else {
					mFooterSwitcher.showPrevious();
				}
				showCampaigns();
			}
			mListView.setSelection(selection);
		}//end onPostExecute
	}
	
	/* Show retrieved campaigns in a list view */
	private void showCampaigns() {
		mListView.setAdapter(new CampaignAdapter(this, campaigns, 
				getMostRecent(campaigns.get(FIRST_CAMPAIGN))));
	}//end showCampaigns
	
	private void showEmptyView() {
		setContentView(View.inflate(this, 
				R.layout.emailsummary_nodata, 
				null));
	}//end showEmptyView
}//end EmailsActivity