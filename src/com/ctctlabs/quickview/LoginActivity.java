
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

/**
 * @author Brendan White
 */

package com.ctctlabs.quickview;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

/** Activity used to login the user. **/
public class LoginActivity extends Activity 
						   implements OnClickListener {

	//login menu items
	private final int MENU_CLEAR_FORM = 0;
	private final int MENU_ABOUT = 1;
	
	//text views for username and password
	private TextView tvUsername;
	private TextView tvPassword;
	
	//switch between login view and loading view
	private ViewSwitcher mSwitcher;
	
	//main login task
	private LoginUserTask mTask;
	
	//binder to allow softkeyboard to be dismissed programatically
	private IBinder mWindowToken;
	
	//has the device been rotated while loading
	private Boolean isRunning;
	
	private Handler tryAgainHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			Bundle data = msg.getData();
			if(data != null) {
			    String username = data.getString("username");
			    String password = data.getString("password");
				tvUsername.setText(username);
				tvPassword.setText(password);
				startLoginTask(username, password);
			}
		}
	};
	
	/* Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		//get rid of the title on this page
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		//setup the views
		setupViews();
		
		//set the content view to the switcher
		setContentView(mSwitcher);
		
		//if the device was rotated while loading show appropriate view
		Boolean wasRunning = (Boolean)getLastNonConfigurationInstance();
		if(wasRunning != null && wasRunning) {
			mSwitcher.showNext();
			this.isRunning = true;
			return;
		}
		
		SharedPreferences prefs = getSharedPreferences(getResources().getString(R.string.str_app_prefs_key), 0);
		boolean isFirstRun = prefs.getBoolean("isFirstRun", true);

		if(!isFirstRun) {
			String defaultUser = prefs.getString("defaultUser", null);
			if(defaultUser != null) {
				String password = prefs.getString(defaultUser, null);
				if(password != null) {
					startLoginTask(defaultUser, password);
				}
			}//end inner if
		}//end outer if
	}//end onCreate
	

	/* Called to setup the views for the Activity. */
	private void setupViews() {
		//setup the first (login) view
		View view0 = View.inflate(this, R.layout.login_view, null);
		
		//get the view's token so keyboard can be dismissed programmatically
		mWindowToken = view0.getWindowToken();
		
		final Button loginButton = (Button)view0.findViewById(R.id.login_button);
		loginButton.setOnClickListener(this);
		
		final Button freeTrialButton = (Button)view0.findViewById(R.id.freetrial_button);
		freeTrialButton.setOnClickListener(this);
		
		tvUsername = (TextView)view0.findViewById(R.id.username_input);
		tvPassword = (TextView)view0.findViewById(R.id.password_input);

			
		Bundle extras = this.getIntent().getExtras();
		if (extras != null && extras.getBoolean("isLoginFailed")) {
			view0.findViewById(R.id.login_failure_tview).setVisibility(View.VISIBLE);
			String username = extras.getString("username");
			tvUsername.setText(username);
			if(!(username.matches("^[\\w.@-]{6,}$")))
				showInvalidCharactersDialog();
		}//end outer if
		
		//setup the second (loading) view
		View view1 = View.inflate(this, R.layout.loading, null);
		view1.findViewById(R.id.rlayout_loading)
			 .setBackgroundColor(Color.rgb(222, 222, 222));
		
		TextView loading = (TextView)view1.findViewById(R.id.tv_progress_loading);
		loading.setText(R.string.str_loggingin);
		
		//create the new view switcher and add the two views created above
		mSwitcher = new ViewSwitcher(this);
		mSwitcher.addView(view0);
		mSwitcher.addView(view1);
	}//end setupViews
	
	/* Pass running status between orientation change. */
	@Override
	public Object onRetainNonConfigurationInstance() {
		return isRunning;
	}//end onRetainNonConfigurationInstance

	/* OnClick handles the login button and the free trial button */
	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.login_button:
				if(validateInput()) {	
					startLoginTask(tvUsername.getText().toString(), 
								   tvPassword.getText().toString());
				}//end if
				break;
				
			case R.id.freetrial_button:
				launchUrl(getString(R.string.str_trialurl));
				break;
		}//end switch	
	}//end onClick
	
	/* Login credentials were invalid, show an error message */
	private void showErrorMessage() {
		//create a new intent to restart the activity
		Intent intent = new Intent(this, LoginActivity.class);
		
		//passed value indicating the login has failed
		intent.putExtra("isLoginFailed", true);
		
		String username = tvUsername.getText().toString();
		if(username.equals("")) {
			SharedPreferences prefs = getSharedPreferences(getResources().getString(R.string.str_app_prefs_key), 0);
			username = prefs.getString("defaultUser", null);
			QuickViewHelper.clearDefaultUser(this);
		}//end if
		
		intent.putExtra("username", username);

		//restart the activity and kill current activity to free up memory
		startActivity(intent);
		finish();
	}//end showErrorMessage
	
	private void startApplication() {
		startActivity(new Intent(this, QuickViewActivity.class));
		finish();
	}//end startApplication
	
	/* Clears any input a user may have entered. */
	private void clearInput() {
		tvUsername.setText("");
		tvPassword.setText("");
	}//end clearInput
	
	/* Given a url, start the browser and open the web page */
	private void launchUrl(String url) {
		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
	}//end launchUrl
	
	/* Validate user input */
	private boolean validateInput() {
		//a username must be input
		if(tvUsername.getText().toString().equals("")) {
			QuickViewHelper.showDialog(this, getString(R.string.str_errortitle), 
					getString(R.string.str_usernameerror), false);
			return false;
		}
		
		//a password must be input
		if(tvPassword.getText().toString().equals("")) {
			QuickViewHelper.showDialog(this, getString(R.string.str_errortitle), 
					getString(R.string.str_passerror), false);
			return false;
		}
		return true;
	}//end validateInput
	
	/* If a username contains invalid characters, show them */
	private void showInvalidCharactersDialog() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.str_errortitle2));
		builder.setPositiveButton(R.string.str_moreinfobtntxt,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						launchUrl(getString(R.string.str_charsurl));
					}
				});
		builder.setNegativeButton(R.string.str_positivebtntxt,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						return;
					}
				});
		builder.setMessage(getString(R.string.str_charserror));
		builder.create().show();
	}//end showInvalidCharactersDialog
	
	/* Create menu options */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    menu.add(0, MENU_CLEAR_FORM, 0, R.string.str_clearformmenu)
	        .setIcon(android.R.drawable.ic_menu_close_clear_cancel);
	    menu.add(0, MENU_ABOUT, 0, R.string.str_aboutmenu)
	        .setIcon(android.R.drawable.ic_menu_info_details);       
        return true;
	}//end onCreateOptionsMenu
	
	/* Called when the menu is opened */
	@Override
	public boolean onMenuOpened(int featureId, Menu menu) {
		if(tvUsername.getText().toString().equals("") && 
				tvPassword.getText().toString().equals(""))
			menu.getItem(0).setEnabled(false);
		else
			menu.getItem(0).setEnabled(true);
		
		return super.onMenuOpened(featureId, menu);
	}//end onMenuOpened

	/* Called when a menu item has been selected. */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
        
        	case MENU_ABOUT:
        		QuickViewHelper.showDialog(this, "About QuickView", 
        						getString(R.string.str_aboutbody), false);
        		return true;

        	case MENU_CLEAR_FORM:
        		clearInput();
        		return true;
        }//end switch
		return false;
	}//end onOptionsItemSelected
	
	private void startLoginTask(String username, String password) {
		hideSoftInput();
		if(QuickViewHelper.checkConnection(this)) {
			mTask = new LoginUser();
			mTask.execute(username, password);
			
			mSwitcher.showNext();
			isRunning = true;
		} else {
			QuickViewHelper.showNoConnectionDialog(this, tryAgainHandler,
												   username, password);
		}//end if-else
	}//end startTASK
	
	private void hideSoftInput() {
		InputMethodManager inputManager = 
			(InputMethodManager)getApplicationContext()
			.getSystemService(Context.INPUT_METHOD_SERVICE);
		
		//hide the soft keyboard
		inputManager.hideSoftInputFromWindow(tvPassword.getWindowToken(), 0);
	}//end hideSoftInput
	
	private class LoginUser extends LoginUserTask {
		@Override
		protected void onPostExecute(Boolean result) {
			isRunning = false;
			
			SharedPreferences prefs = getSharedPreferences(getResources().getString(R.string.str_app_prefs_key), 0);
		    SharedPreferences.Editor prefsEditor = prefs.edit();
			
			if(prefs.getBoolean("isFirstRun", true)) {
				prefsEditor.putBoolean("isFirstRun", false);
			}
			
			if(result) {
				prefsEditor.putString("defaultUser", username);
				prefsEditor.putString(username, password);
				prefsEditor.commit();
				startApplication();
			} else {
				showErrorMessage();
			}
			mTask = null;
		}//end on postExecute
	}//end LoginUser
}//end LoginActivity
