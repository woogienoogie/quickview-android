
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

/**
 * @author Brendan White
 */

package com.ctctlabs.quickview;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.widget.TabHost;

public class QuickViewActivity extends TabActivity {
	
	// version string, used for comparison purposes in checkForUpdates()
	private final String QUICKVIEW_VERSION = "1.0";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
        
    	super.onCreate(savedInstanceState);
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
    	
    	String updateUrl = checkForUpdate();
    	if(updateUrl != null) {
    		showUpdateDialog(updateUrl);
    	}//end if

    	TabHost host = getTabHost();

		host.addTab(host.newTabSpec("Emails_Spec")
						.setIndicator("Emails", getResources().getDrawable(R.drawable.emails_selector))
						.setContent(new Intent(this, EmailSummaryActivity.class)));
		host.addTab(host.newTabSpec("Lists_Spec")
						.setIndicator("Contact Lists", getResources().getDrawable(R.drawable.lists_selector))
						.setContent(new Intent(this, ContactListsActivity.class)));
		host.addTab(host.newTabSpec("Contacts_Spec")
						.setIndicator("Contacts", getResources().getDrawable(R.drawable.contacts_selector))
						.setContent(new Intent(this, ContactsActivity.class)));
    }//end onCreate
	
	private String checkForUpdate() {
		
		// this function checks a text file at the specified URL for updates to the app, and presents a dialog box to update if it sees one.
		// the text file format is:
		
		// VERSION,URL
		
		// where VERSION is different from the current app value of QUICKVIEW_VERSION,
		// and URL is where to direct a press on the str_checkforupdatebtntxt button.
		
		// Example file contents:
		// 1.0beta1,http://example.com/current_app.apk
		
		HttpClient httpClient = new DefaultHttpClient();
		httpClient.getParams().setParameter(HttpConnectionParams.CONNECTION_TIMEOUT, 3000);
		HttpGet request = new HttpGet(""); // TODO enter a URL containing a text file to check for updates. 
		HttpResponse response = null;
		
		try {
			response = httpClient.execute(request);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (response != null &&
					response.getStatusLine().getStatusCode()== HttpStatus.SC_OK) {
				 InputStream stream;
				try {
					stream = response.getEntity().getContent();
					String stringResponse = convertStreamToString(stream);
					String[] splits = stringResponse.split(",");
					if(splits.length > 1) {
						String version = splits[0];
						if(!version.trim().equals(QUICKVIEW_VERSION)) {
							return splits[1]; //url for download
						}
					}
					stream.close();
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} 
			} else {
				return null;
			}
		}
		return null;
	}//end checkForUpdate
	
	private static String convertStreamToString(InputStream is) {
        /*
         * To convert the InputStream to String we use the BufferedReader.readLine()
         * method. We iterate until the BufferedReader return null which means
         * there's no more data to read. Each line will appended to a StringBuilder
         * and returned as String.
         */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
 
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
	
	private void showUpdateDialog(final String updateUrl) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.str_checkforupdatetitle);
		builder.setPositiveButton(R.string.str_checkforupdatebtntxt,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl)));
						finish();
					}
				});
		builder.setNegativeButton(R.string.str_negativebtntxt,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						return;
					}
				});
		builder.setMessage(R.string.str_checkforupdatebody);
		builder.create().show();
	}//end showInvalidCharactersDialog
}//end QuickViewActivity
