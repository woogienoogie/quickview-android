
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

/**
 * @author Brendan White
 */
package com.ctctlabs.quickview;

import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.auth.InvalidCredentialsException;
import org.apache.http.client.ClientProtocolException;

import com.ctctlabs.quickview.webservice.Contact;
import com.ctctlabs.quickview.webservice.ContactList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

/** Activity to display a selected contacts details. From this activity a user
 *  can choose to edit a contact, delete a contact, or email a contact.  **/
public class ContactDetailsActivity extends Activity {
	
	//menu items
	private final int MENU_EDIT_CONTACT = 0;
	private final int MENU_DELETE_CONTACT = 1;
	private final int MENU_EMAIL_CONTACT = 2;
	
	//intent for editing a contact
	static final String ACTION_EDIT_CONTACT = 
						"com.ctctlabs.quickview.ContactDetailsActivity.EDIT_CONTACT";
	
	//incoming intent action types
	private final String CONTACTS_ACTION = 
						 "com.ctctlabs.quickview.ContactsActivity.SHOW_CONTACT_DETAILS";
	private final String LISTS_ACTION = 
						 "com.ctctlabs.quickview.ListDetailsActivity.SHOW_CONTACT_DETAILS";
	private final String EMAILS_ACTION = 
						 "com.ctctlabs.quickview.EmailDetailsListActivity.SHOW_CONTACT_DETAILS";
	
	//contact that is accessible from add/edit contact activity
	public static Contact contact;
	private String contactLink= "";
	
	//text views and string values for storing and displaying a contacts details
	private TextView email;			private String emailValue = "";
	private TextView email2; //used for black/gray display		
	private TextView name;			private String nameValue = "";
	private TextView address1;		private String address1Value = "";
	private TextView address2;		private String address2Value = "";
	private TextView address3;		private String address3Value = "";
	private TextView homePhone;		private String homePhoneValue = "";
	private TextView workPhone;		private String workPhoneValue = "";
	private TextView jobTitle;		private String jobTitleValue = "";
	private TextView company;		private String companyValue = "";
	private TextView lists;
	
	//async tasks for getting a contact and deleting a contact
	private GetContactTask mTask;
	private DeleteContactTask mDeleteTask;
	
	private ViewSwitcher mSwitcher;
	
	@Override
	public Object onRetainNonConfigurationInstance() {
		return contact;
	}//end onRetainNonConfigurationInstance

	//setup the views for the activity
	private void setupViews() {
		View view = View.inflate(this, R.layout.contactdetails, null);
		
		email = (TextView)view.findViewById(R.id.tv_email1_contactdetails);
		email2 = (TextView)view.findViewById(R.id.tv_email2_contactdetails);
		name = (TextView)view.findViewById(R.id.tv_name_contactdetails);
		address1 = (TextView)view.findViewById(R.id.tv_address1_contactdetails);
		address2 = (TextView)view.findViewById(R.id.tv_address2_contactdetails);
		address3 = (TextView)view.findViewById(R.id.tv_address3_contactdetails);
		homePhone = (TextView)view.findViewById(R.id.tv_homephone_contactdetails);
		workPhone = (TextView)view.findViewById(R.id.tv_workphone_contactdetails);
		jobTitle = (TextView)view.findViewById(R.id.tv_jobtitle_contactdetails);
		company = (TextView)view.findViewById(R.id.tv_company_contactdetails);
		lists = (TextView)view.findViewById(R.id.tv_lists_contactdetails);
		
		mSwitcher = new ViewSwitcher(this);
		mSwitcher.addView(QuickViewHelper.getLoadingView(this, "Contact Details"));
		mSwitcher.addView(view);
	}//end setupViews
	
	//set the detail values if they are available
	private void setValues() {
		
		// flag for having any contact info (aside from email)
		boolean hasAnyContactDetails = false;
		
		//pieces of email address
		String[] splits = emailValue.split("@");
		
		//if there was a successful split
		if(splits.length == 2) {
			email.setText(splits[0] + "@");
			email2.setText(splits[1]);
		} else {
			email2.setVisibility(View.GONE);
			email.setText(emailValue);
		}//end if-else
		
		//if there is a name value, set it
		if(!(nameValue.trim().equals(""))) {
			name.setText(nameValue);
			hasAnyContactDetails = true;
		} else { //otherwise hide 
			LinearLayout nameLayout = 
						(LinearLayout)findViewById(R.id.lo_name_contactdetails);
			nameLayout.setVisibility(View.GONE);
		}//end if-else
		
		//if there is a address associated, set it
		if(!(address1Value.trim().equals(""))) {
			address1.setText(address1Value);
			hasAnyContactDetails = true;
		} else { //otherwise hide
			address1.setVisibility(View.INVISIBLE);
		}//end if-else
		
		//if there is a address2 value, set it
		if(!(address2Value.trim().equals(""))) {
			address2.setText(address2Value);
			hasAnyContactDetails = true;
		} else { //otherwise hide it
			address2.setVisibility(View.INVISIBLE);
		}//end if-else

		if(!(address3Value.trim().equals(""))) {
			address3.setText(address3Value);
			hasAnyContactDetails = true;
		} else {
			address3.setVisibility(View.INVISIBLE);
		}//end if-else

		//if there are no address values at all hide entire section
		if(address1Value.trim().equals("") &&
				address2Value.trim().equals("") &&
				address3Value.trim().equals("")) {
			LinearLayout addressLayout = 
						(LinearLayout)findViewById(R.id.lo_address_contactdetails);
			addressLayout.setVisibility(View.GONE);
		}//end if-else
		
		//if there is a home phone value, set it
		if(!(homePhoneValue.trim().equals(""))) {
			homePhone.setText(homePhoneValue);
			hasAnyContactDetails = true;
		} else { //otherwise hide it
			LinearLayout homeLayout = 
						(LinearLayout)findViewById(R.id.lo_phone_contactdetails);
			homeLayout.setVisibility(View.GONE);
		}//end if-else
		
		//if there is a work phone value, set it
		if(!(workPhoneValue.trim().equals(""))) {
			workPhone.setText(workPhoneValue);
			hasAnyContactDetails = true;
		} else { //otherwise hide the section
			LinearLayout workLayout = 
						(LinearLayout)findViewById(R.id.lo_work_contactdetails);
			workLayout.setVisibility(View.GONE);
		}//end if-else
		
		//if there is a job title value, set it
		if(!(jobTitleValue.trim().equals(""))) {
			jobTitle.setText(jobTitleValue);
			hasAnyContactDetails = true;
		} else { //otherwise hide the section
			LinearLayout jobTitleLayout = 
						(LinearLayout)findViewById(R.id.lo_jobtitle_contactdetails);
			jobTitleLayout.setVisibility(View.GONE);
		}//end if-else
		
		//if there is a value for company, set it
		if(!(companyValue.trim().equals(""))) {
			company.setText(companyValue);
			hasAnyContactDetails = true;
		} else { //otherwise hide it
			LinearLayout companyLayout = 
						(LinearLayout)findViewById(R.id.lo_company_contactdetails);
			companyLayout.setVisibility(View.GONE);
		}//end if-else

		// if there are no contact details at all, hide the whole thing
		if (! hasAnyContactDetails) {
			ImageView line2ImageView = 
				(ImageView)findViewById(R.id.line2_contactdetails);
			line2ImageView.setVisibility(View.GONE);
		}
		
		String listString = "";
		try {
			listString = getListsString();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		lists.setText(listString);		
	}//end setValues
	
	/* Retrieves the lists for the contact(if any) and return a string to display
	 * on the details page */
	private String getListsString() throws ClientProtocolException, IOException {
		String listString = "";
		ArrayList<ContactList> lists = 
				 (ArrayList<ContactList>) contact.getAttribute("ContactLists");
		if(lists != null) {
			for(ContactList list : lists) {
				listString += list.getAttribute("Name").toString() + "\n";
			}//end for
		}//end if
		return listString;
	}//end getListsString
	
	/* Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    //no title
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		//first setup the views
		setupViews();
		
		//set the content view
		setContentView(mSwitcher);
		
		//if there was an orientation change, get the passed contact
		Contact passedContact = (Contact) getLastNonConfigurationInstance();
		if(passedContact != null) {
			contact = passedContact;
			setContactAttributes();
			mSwitcher.showNext();
		} else {
			mTask = new GetContactTask();
			mTask.execute();
		}//end if-else		
	}//end onCreate
	
	/* Set all of the contact attributes, if there is no attribute for a given
	 * column then an empty string will be returned */
	private void setContactAttributes() {
		try {
			emailValue = contact.getAttribute("EmailAddress").toString();
			nameValue = contact.getAttribute("Name").toString();
			address1Value = contact.getAttribute("Addr1").toString();
			address2Value = contact.getAttribute("Addr2").toString();
			address3Value = contact.getAttribute("Addr3").toString();
			homePhoneValue = contact.getAttribute("HomePhone").toString();
			workPhoneValue = contact.getAttribute("WorkPhone").toString();
			jobTitleValue = contact.getAttribute("JobTitle").toString();
			companyValue = contact.getAttribute("CompanyName").toString();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		setValues();
	}//end setContactAttributes
	
	/* GetContactTask */
	private class GetContactTask extends AsyncTask<Void, Integer, 
											    	   Contact> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}//end onPreExecute

		@Override
		protected Contact doInBackground(Void... arg0) {
			contactLink = null;
			Intent intent = getIntent();
			if(intent != null) {
				String action = intent.getAction();
				if(action == null) {
					contactLink = intent.getStringExtra("CONTACT_ID");
				} else {
					if(action.equals(CONTACTS_ACTION)) {
						contactLink = intent.getStringExtra(ContactsActivity.EXTRA_CONTACT_ID);
					} else if(action.equals(LISTS_ACTION)) {
						contactLink = intent.getStringExtra(ListDetailsActivity.EXTRA_CONTACT_ID);
					} else if(action.equals(EMAILS_ACTION)) {
						contactLink = intent.getStringExtra(EmailDetailsListActivity.EXTRA_CONTACT_ID);
					}//end inner if-else
				}//end outer if-else
				
			}//end outer if
			
			if(contactLink != null) {
				try {
					contact = Connection.getConn().getContactByLink(contactLink);
				} catch (InvalidCredentialsException e) {
					e.printStackTrace();
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return contact;
		}//end doInBackground

		@Override
		protected void onPostExecute(Contact result) {
			super.onPostExecute(result);
			if(contact != null) {
				setContactAttributes();
			} //end if
			mSwitcher.showNext();
			mTask = null;
		}//end onPostExecute	
	}//end GetContactTask
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) { 
		menu.add(0, MENU_EDIT_CONTACT, 0, "Edit contact")
			.setIcon(android.R.drawable.ic_menu_edit);
		menu.add(0, MENU_DELETE_CONTACT, 0, "Delete contact")
			.setIcon(android.R.drawable.ic_menu_delete);
		menu.add(0, MENU_EMAIL_CONTACT, 0, "Email contact")
			.setIcon(android.R.drawable.ic_menu_send);
		
        return true;
	}//end onCreateOptionsMenu
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
    		case MENU_EDIT_CONTACT:
    			startActivityForResult(new Intent(ACTION_EDIT_CONTACT), 0);
    			return true;
		
			case MENU_DELETE_CONTACT:
				showConfirmDelete();
        		return true;
        		
			case MENU_EMAIL_CONTACT:
				emailContact();
        		return true;       
        }//end switch        
		return false;
	}//end onOptionsItemSelected
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == Activity.RESULT_OK) {
			Intent intent = new Intent(this, ContactDetailsActivity.class);
			intent.putExtra("CONTACT_ID", contactLink);
			finish();
			startActivity(intent);
		}//end if
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		if(id == 1) {
			ProgressDialog mDialog = new ProgressDialog(this);
			mDialog.setMessage("Deleting contact...");
			mDialog.setIndeterminate(true);
			mDialog.setCancelable(false);
			return mDialog;
		}
		return null;
	}//end onCreateDialog
	
	private void showConfirmDelete() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Delete Contact");
		builder.setCancelable(false);
		builder.setPositiveButton("Delete Contact",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						deleteContact();
						return;
					}
				});
		builder.setNegativeButton(R.string.str_negativebtntxt,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						return;
					}
				});
		builder.setMessage("This contact will be deleted from all contact lists.");
		builder.create().show();
	}//end showLogOutDialog
	
	/* DeleteContactTask */
	private class DeleteContactTask extends AsyncTask<Void, Integer, Boolean> {
		@Override
		protected Boolean doInBackground(Void... arg0) {
			boolean wasDeleted = false;
			try {
				wasDeleted = contact.delete();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return wasDeleted;
		}//end doInBackground

		@Override
		protected void onPostExecute(Boolean wasDeleted) {
			if(wasDeleted) {
				finish();
			} else {
				dismissDialog(1);
			}//end if-else	
			mDeleteTask = null;
		}//end onPostExectute
		
	}//end DeleteContactTask
	
	private void deleteContact() {
		showDialog(1);
		mDeleteTask = new DeleteContactTask();
		mDeleteTask.execute();
	}//end deleteContact
	
	private void emailContact() {
		final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
		emailIntent.setType("plain/text");
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{emailValue});
		startActivity(Intent.createChooser(emailIntent, "Send mail..."));	
	}//end emailContact
}//end ContactDetailsActivity
