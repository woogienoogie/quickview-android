
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

/**
 * @author Brendan White
 */

package com.ctctlabs.quickview;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.auth.InvalidCredentialsException;
import org.apache.http.client.ClientProtocolException;

import com.ctctlabs.quickview.webservice.ContactIterator;
import com.ctctlabs.quickview.webservice.ContactList;
import com.ctctlabs.quickview.webservice.ModelObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import android.widget.AdapterView.OnItemClickListener;

/** Activity to display a selected contact list's members. Members are loaded
 *  50 at a time. From this view you can add a contact to the currently selected
 *  list or delete a list. **/
public class ListDetailsActivity extends Activity
								 implements OnItemClickListener,
								 			OnClickListener {
	
	private final int LISTS_PER_PAGE = 50;
	private final int FIRST_PAGE = 1;
	
	//menu items for list members
	private final int MENU_ADD_TO_LIST = 0;
	private final int MENU_DELETE_LIST = 1;
	
	//intent variables for adding a new contact to the selected list
	static final String ACTION_ADD_TO_LIST = 
						"com.ctctlabs.quickview.ListDetailsActivity.ADD_TO_LIST";
	static final String EXTRA_LIST_NAME = 
						"com.ctctlabs.quickview.ListDetailsActivity.extra.LIST_NAME";
	
	//intent variables for showing the contact details of a contact in the current list
	static final String ACTION_SHOW_CONTACT_DETAILS = 
						"com.ctctlabs.quickview.ListDetailsActivity.SHOW_CONTACT_DETAILS";
	static final String EXTRA_CONTACT_ID = 
						"com.ctctlabs.quickview.ListDetailsActivity.extra.CONTACT_ID";
	
	//current context
	Context context = this;
	
	//heading that shows the appropriate listname
	private String headingText;
	
	//list view and contacts adapter
	private ListView mListView;
	
	//the main switcher and the switcher for loading more members
	private ViewSwitcher mSwitcher;
	private ViewSwitcher mFooterSwitcher;
	
	//members holds list members of currently selected list
	private ArrayList<HashMap<String, String>> members;
	private ContactIterator contactIter;
	
	//task to get the list members
	private GetMembersTask mTask;
	private GetMembersTask mLoadMoreTask;
	private DeleteListTask mDeleteListTask;
	
	//last page accessed and currently selected list
	private int page = 0;
	private int selection = 0;
	
	/* Class to hold configuration data to be passed on orientation change */
	private class MembersConfiguration extends Object {
		
		//the campaigns that have been loaded, and the iterator
		private ArrayList<HashMap<String, String>> members;
		private ContactIterator contactIter;
		
		//the last page(number) loaded and the current selection
		private int page;
		private int selection;
		
		private boolean wasRunning; //was the load more task running when
									//the orientation changed?
		
		//constructor #1
		MembersConfiguration(ArrayList<HashMap<String, String>> _members,
				ContactIterator _contactIter, int _page,
							  boolean _wasRunning, int _selection) {
			this.members = _members; 	this.contactIter = _contactIter;
			this.page = _page; 			this.wasRunning = _wasRunning;
			this.selection = _selection;
		}//end constructor
	}//end CampaignConfiguration
	
	//progress for deleting lists
	private ProgressDialog deleteProgress;
	
	@Override
	public Object onRetainNonConfigurationInstance() {
		MembersConfiguration config;
		
		//if the main task was running when orientation changed return null
		//so the activity is restarted and cancel the current thread
		if(mTask != null && mTask.getStatus() == AsyncTask.Status.RUNNING) {
			mTask.cancel(true);
			return null;
		}//end if
		
		//if the load more task was running, let the new activity know through
		//"wasRunning"
		boolean wasRunning = false;
		if(mLoadMoreTask != null && mLoadMoreTask.getStatus() == 
								    AsyncTask.Status.RUNNING) {
			mLoadMoreTask.cancel(true);
			wasRunning = true;
		}//end if
		
		//get the configuration and pass it
		config = new MembersConfiguration(members, contactIter, 
										   page, wasRunning, selection);	
		return config;
	}//end onRetainNonConfigurationInstance
	
	private void setupViews() { 
		TextView heading = new TextView(this);
		heading.setPadding(6, 0, 0, 0);
		heading.setBackgroundColor(Color.rgb(105, 105, 105));
		heading.setText(headingText);
		
		//setup the list view and set listener
        mListView = new ListView(this);
        mListView.addHeaderView(heading, null, false);
        mListView.setOnItemClickListener(this);
		mListView.setBackgroundColor(Color.rgb(222, 222, 222));
		mListView.setCacheColorHint(0x00000000);
        
        //setup the switcher
		mSwitcher = new ViewSwitcher(this);
		
		//and the footer view
		mFooterSwitcher = new ViewSwitcher(this);
		
		Button loadButton = (Button)View.inflate(this, R.layout.loadmore_button, null);
		loadButton.setText("Load more members...");
		loadButton.setOnClickListener(this);
		
		mFooterSwitcher.addView(loadButton);
		mFooterSwitcher.addView(View.inflate(this, R.layout.loading_footer, null));
	}//end setupViews
	
	private void showEmptyListView() {
		View emptyListView = View.inflate(this, R.layout.listmembers_nodata, 
									   null);
		TextView listName = 
			(TextView)emptyListView.findViewById(R.id.tv_emaildetailslist_unavailable_heading);
		listName.setText(headingText);
		
		setContentView(emptyListView);
	}//end noDataView
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //no title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        //get the list name
		Bundle extras = getIntent().getExtras();
		if(extras != null)
			headingText = extras.getString(ContactListsActivity.EXTRA_LISTNAME);
        
        //setup the views
		setupViews();
		
		//holds list member data to be passed between orientation changes
		MembersConfiguration passedConfig = 
			(MembersConfiguration) getLastNonConfigurationInstance();
		
		if(passedConfig == null) {
			mSwitcher.addView(QuickViewHelper.getLoadingView(this, headingText));
			mTask = new GetMembersTask();
			mTask.execute();
			
		} else {
			
			page = passedConfig.page;
			selection = passedConfig.selection;
			
			contactIter = passedConfig.contactIter;
			members = passedConfig.members;
			
			if(contactIter.hasNextPage()) {
				mListView.addFooterView(mFooterSwitcher);
			}//end if
			
			showListMembers();
			
			//the load more campaigns task was running when the orientation
			if(passedConfig.wasRunning) {
				//move the selection and show the loading view
				mListView.setSelection(page * LISTS_PER_PAGE);
				mFooterSwitcher.showNext();
				
				//start a new task
				mLoadMoreTask = new GetMembersTask();
				mLoadMoreTask.execute();
			}
		}//end else

        mSwitcher.addView(mListView);
		setContentView(mSwitcher);
    }//end onCreate
    
    /* GetMembersTask */
    private class GetMembersTask extends AsyncTask<Void, Integer, 
    										       ArrayList<HashMap<String, String>>> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if(members == null)
				members = new ArrayList<HashMap<String, String>>();
		}//end onPreExecute
    	
    	@Override
		protected ArrayList<HashMap<String, String>> doInBackground(
							Void... params) {
			
    		ArrayList<ModelObject> loadedMembers = null;
			HashMap<String, String> member = null;
			String contactId = "";
			
			try {
				
				Bundle extras = getIntent().getExtras();
				String listLink = "";
				if(extras != null)
					listLink = extras.getString(ContactListsActivity.EXTRA_LISTLINK);
				
				//if it is the first
				if(contactIter == null)
					contactIter = Connection.getConn().getContactListMembers(listLink);
				else
					contactIter.loadNextPage();
				
				//get the first page of the list members
				loadedMembers = contactIter.getLoadedEntries();
				
				//if there are none don't proceed. move to onPostExecute
				//if(loadedMembers.isEmpty())
				//	return null;
				
				int i = selection = page * LISTS_PER_PAGE;
				//for every loaded member
				for(; i < loadedMembers.size(); i++) {
					//current member contactId
					contactId = loadedMembers.get(i).getAttribute("ContactId").toString();
					
					//holds a member
					member = new HashMap<String, String>();
					
					//add member details (contctId and Email)
					member.put("ID", QuickViewHelper.getURLPath(contactId));
					member.put("EMAIL", loadedMembers.get(i).getAttribute("EmailAddress").toString());
					
					//add member to the list of members
					members.add(member);
				}//end for
					
			} catch (InvalidCredentialsException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			page++;
			return members;
		}//end doInBackground

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
			super.onPostExecute(result);
			if(result.isEmpty()) {
				showEmptyListView();
				return;
			}//end if
			
			if(page == FIRST_PAGE) {
				if(contactIter.hasNextPage()) {
					mListView.addFooterView(mFooterSwitcher);
				} 
				showListMembers();
				mSwitcher.showNext();
			} else {
				if(!contactIter.hasNextPage()) {
					mListView.removeFooterView(mFooterSwitcher);
				} else {
					mFooterSwitcher.showPrevious();
				}
				showListMembers();
			}
			mListView.setSelection(selection);
		}//end onPostExecute
    }//end RetrieveMembersTask
    
    /* Delete List Task */
    private class DeleteListTask extends AsyncTask<Void, Integer, 
    										       Boolean> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}//end onPreExecute

		@Override
		protected Boolean doInBackground(Void... arg0) {
	    	boolean wasDeleted = false;
	    	
	    	//the contact list to delete
	    	ContactList listToDelete = null;
			
	    	//get the list link if there is one
	    	Bundle extras = getIntent().getExtras();
	    	String listLink = "";
	    	if(extras != null)
	    		listLink = extras.getString(ContactListsActivity.EXTRA_LISTLINK);
	    		
	    	try {
				if(!listLink.equals("")){
		    		listToDelete = Connection.getConn().getContactList(listLink);
					wasDeleted = listToDelete.delete();
				}
			} catch (InvalidCredentialsException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			//if the list was successfully deleted set the result and return
			if(wasDeleted) {
				Intent intent = new Intent();
				intent.putExtra("LIST_LINK", listLink);
				setResult(Activity.RESULT_OK, intent);
			}//end if
	    	
			return wasDeleted;
		}//end doInBackground

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			//get rid of the dialog regardless of success
			deleteProgress.dismiss();
			//if it was successful end the activity
			if(result) {
				finish();
			}//end if
		}//end onPostExecute
    }//end DeleteListsTask
    
	/* Before a list is deleted, confirm the delete */
	private void showConfirmDeleteDialog() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.str_listmemconfirmtitle));
		builder.setPositiveButton(R.string.str_listmemconfirmbtntxt,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						startProgressDialog();
						mDeleteListTask = new DeleteListTask();
						mDeleteListTask.execute();
					}
				});
		builder.setNegativeButton(R.string.str_negativebtntxt,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						return;
					}
				});
		builder.setMessage(getString(R.string.str_listmemconfirmbody));
		builder.create().show();
	}//end showInvalidCharactersDialog
	
	private void startProgressDialog() {
		deleteProgress = ProgressDialog.show(this, null, "Deleting List...");
	}//end startProgressDialog
    
    private void showListMembers() {
    	mListView.setAdapter(new ContactsAdapter(this, members));
    }//end showListMembers
    
	@Override
	public void onItemClick(AdapterView<?> adapterView, 
						    View view, int position, long positionAsLong) {
		//adjust for added headerview
		position--;
		
		Intent intent = new Intent(ACTION_SHOW_CONTACT_DETAILS);
		intent.putExtra(EXTRA_CONTACT_ID, members.get(position).get("ID"));
		startActivity(intent);
	}//end onItemClick
    
	private void addContactToList() {
		//get the list link if there is one
    	Bundle extras = getIntent().getExtras();
    	Intent intent = new Intent(ACTION_ADD_TO_LIST);
    	intent.putExtra(EXTRA_LIST_NAME, 
    			extras.getString(ContactListsActivity.EXTRA_LISTLINK));
    	startActivityForResult(intent, 0);
    }//end addContactToList
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
			if(resultCode == Activity.RESULT_OK) {
				if(members.isEmpty()) {
					Bundle extras = getIntent().getExtras(); //extras from incoming activity
					Intent intent = new Intent(this, ListDetailsActivity.class);
					intent.putExtra(ContactListsActivity.EXTRA_LISTLINK, extras.getString(ContactListsActivity.EXTRA_LISTLINK));
					intent.putExtra(ContactListsActivity.EXTRA_LISTNAME, extras.getString(ContactListsActivity.EXTRA_LISTNAME));
					finish();
					startActivity(intent);
				} else {
					HashMap<String, String> addedContact = new HashMap<String, String>();
					addedContact.put("EMAIL", data.getStringExtra("EMAIL_ADDRESS"));
					addedContact.put("ID", data.getStringExtra("CONTACT_ID"));
					members.add(0, addedContact);
					showListMembers();
				}//end if-else
			}//end if
	}//end onActivityResult
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, MENU_ADD_TO_LIST, 0, "Add new contact to list")
			.setIcon(android.R.drawable.ic_menu_add);
		menu.add(0, MENU_DELETE_LIST, 0, "Delete list")
		.setIcon(android.R.drawable.ic_menu_delete);
        return true;
	}//end onCreateOptionsMenu
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
        
    		case MENU_ADD_TO_LIST:
    			addContactToList();
    			return true;
		
			case MENU_DELETE_LIST:
				showConfirmDeleteDialog();
        		return true;
        }//end switch    
		return false;
	}//end onOptionsItemSelected

	@Override
	public void onClick(View view) {
		switch(view.getId()) {
		case R.id.btn_loadmorecontacts:
			mFooterSwitcher.showNext();
			mLoadMoreTask = new GetMembersTask(); 
			mLoadMoreTask.execute();
			break;
		}//end switch
	}//end onClick
}//end ListDetailsActivity
