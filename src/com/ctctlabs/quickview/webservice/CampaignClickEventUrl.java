
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

package com.ctctlabs.quickview.webservice;

/**
 * Holds data regarding a Click Event Url
 * 
 * @author Huan Lai
 */
public class CampaignClickEventUrl {
	private String url;
	private String link;
	private int clicks;
	
	public CampaignClickEventUrl(String url, String link, int clicks) {
		this.url = url;
		this.link = link;
		this.clicks = clicks;
	}
	
	public CampaignClickEventUrl() {
	}
	
	public String getUrl() {
		return url;
	}
	public String getLink() {
		return link;
	}
	public int getClicks() {
		return clicks;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setClicks(int clicks) {
		this.clicks = clicks;
	}
	
}
