
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

package com.ctctlabs.quickview.webservice;

import java.util.ArrayList;
import java.util.HashMap;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 * Model object encapsulating an individual campaign entry.
 * Has the following attributes:
 * 		CampaignId (preloaded by CTCTConnection.getCampaigns())
 * 		Link (preloaded by CTCTConnection.getCampaigns())
 * 		Updated (preloaded by CTCTConnection.getCampaigns())
 * 		Name (preloaded by CTCTConnection.getCampaigns())
 * 		Date (preloaded by CTCTConnection.getCampaigns())
 * 		LastEditDate
 * 		LastRunDate
 * 		NextRunDate
 * 		Status (preloaded by CTCTConnection.getCampaigns())
 * 		Sent
 * 		Opens
 * 		Clicks
 * 		Bounces
 * 		Forwards
 * 		OptOuts
 * 		Urls
 * 
 * @author Huan Lai
 *
 */
public class Campaign extends ModelObject {
	private boolean inUrls;
	private CampaignClickEventUrl currentUrl;
		
	Campaign(HashMap<String, Object> attributes, CTCTConnection connection) {
		super(attributes, connection);
		inUrls = false;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void startElement(String namespaceURI, String localName,
			String qName, Attributes atts) throws SAXException {
		super.startElement(namespaceURI, localName, qName, atts);
		
		if(localName.equals("Urls")) {
			inUrls = true;
		} else if(inUrls && localName.equals("Url")) {
			if(!attributes.containsKey("Urls")) {
				attributes.put("Urls", new ArrayList<CampaignClickEventUrl>());
			}
			
			currentUrl = new CampaignClickEventUrl();
			currentUrl.setLink(atts.getValue("id").replaceAll("http://api.constantcontact.com", ""));
		} 
	}
	
	@Override
	public void endElement(String namespaceURI, String localName, String qName)
			throws SAXException {
		super.endElement(namespaceURI, localName, qName);
		
		if(localName.equals("source")) {
			inSource = false;
		}
		
		if(localName.equals("Urls")) {
			inUrls = false;
		}
		
		// Skip the source section of the response
		if(!inSource && !inUrls) {
			if(localName.equals("id")) {
				attributes.put("CampaignId", currentString);
			} else if(localName.equals("updated")) {
				attributes.put("Updated", currentString);
			} else if(localName.equals("Name")) {
				attributes.put("Name", currentString);
			} else if(localName.equals("Date")) {
				attributes.put("Date", currentString);
			} else if(localName.equals("Status")) {
				attributes.put("Status", currentString);
			} else if(localName.equals("LastEditDate")) {
				attributes.put("LastEditDate", currentString);
			} else if(localName.equals("LastRunDate")) {
				attributes.put("LastRunDate", currentString);
			} else if(localName.equals("NextRunDate")) {
				attributes.put("NextRunDate", currentString);
			} else if(localName.equals("Sent")) {
				attributes.put("Sent", currentString);
			} else if(localName.equals("Opens")) {
				attributes.put("Opens", currentString);
			} else if(localName.equals("Clicks")) {
				attributes.put("Clicks", currentString);
			} else if(localName.equals("Bounces")) {
				attributes.put("Bounces", currentString);
			} else if(localName.equals("Forwards")) {
				attributes.put("Forwards", currentString);
			} else if(localName.equals("OptOuts")) {
				attributes.put("OptOuts", currentString);
			}
		} else if(inUrls) {
			if(localName.equals("Value")) {
				currentUrl.setUrl(currentString);
			} else if(localName.equals("Clicks")) {
				currentUrl.setClicks(Integer.parseInt(currentString));
			} else if(localName.equals("Url")) {
				ArrayList<CampaignClickEventUrl> urls = (ArrayList<CampaignClickEventUrl>) attributes.get("Urls");
				urls.add(currentUrl);
			}	
		}
	}
}
