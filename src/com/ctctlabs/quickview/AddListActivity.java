
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

/**
 * @author Brendan White
 */
package com.ctctlabs.quickview;

import java.io.IOException;
import java.util.HashMap;

import org.apache.http.auth.InvalidCredentialsException;
import org.apache.http.client.ClientProtocolException;

import com.ctctlabs.quickview.webservice.ContactList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

/** Activity to create a new contact list. **/
public class AddListActivity extends Activity 
							 implements OnClickListener {
    
	private EditText etListName;
	private ProgressDialog addProgress;

	private String listName = "";
	private String listLink = "";
	
	private AddListTask mAddTask;
	
	/* Setup the views for the Activity */
	private void setupViews() {
        //setup the button for adding a list
		Button btnDone = (Button)findViewById(R.id.btn_done_addlist);
        btnDone.setOnClickListener(this);
        
        //setup the button for canceling the add
        Button btnCancel = (Button)findViewById(R.id.btn_cancel_addlist);
        btnCancel.setOnClickListener(this);
        
        //edit text for list name input
        etListName = (EditText)findViewById(R.id.et_addlist);
	}//end setupViews
	
	/* Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //no title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        //set the content view
        setContentView(R.layout.add_list_view);
        
        //setup the views
        setupViews();
    }//end onCreate
    
	/* Add Contact List Task */
	private class AddListTask extends AsyncTask<Void, Integer, 
												Boolean> {
		@Override
		protected Boolean doInBackground(Void... arg0) {
			boolean wasAdded = false;
			
			//get the listname
			String name = etListName.getText().toString();
			
			//create attribute to be added 
			HashMap<String, Object> attributes = new HashMap<String, Object>();
			attributes.put("Name", name);
			
			//create then new contact list
			ContactList list = null;
			try {
				list = Connection.getConn().createContactList(attributes);
				list.commit();
				
				//if the list was successfully added
				if(list.hasAttribute("ContactListId")) {
					listName = name;
					listLink = (String) list.getAttribute("Link");
					wasAdded = true;
				}//end if
			} catch (InvalidCredentialsException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return wasAdded;
		}//end doInBackground

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			//dispose the progress dialog
			addProgress.dismiss();
			if(result) {
				Intent intent = new Intent();
				intent.putExtra("LIST_NAME", listName);
				intent.putExtra("LIST_LINK", listLink);
				setResult(Activity.RESULT_OK, intent);
				finish();
			} else {
				etListName.setText("");
				showDuplicateListDialog();
			}//end else
		}//end onPostExecute
	}//end AddListTask
	
	private void showDuplicateListDialog() {
		QuickViewHelper.showDialog(this, getString(R.string.str_errortitle), 
								   getString(R.string.str_addlistbody), false);
	}//end showDuplicateListDialog

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.btn_done_addlist:
			addProgress = ProgressDialog.show(this, null, "Adding list...");
			mAddTask = new AddListTask();
			mAddTask.execute();
			break;
			
		case R.id.btn_cancel_addlist:
			finish();
			break;		
		}//end switch
	}//end onClick
}//end AddListActivity


