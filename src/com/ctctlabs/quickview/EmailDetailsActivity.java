
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

/**
 * @author Brendan White
 */

package com.ctctlabs.quickview;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.auth.InvalidCredentialsException;
import org.apache.http.client.ClientProtocolException;

import com.ctctlabs.quickview.webservice.Campaign;

import android.app.Activity;
import android.content.Intent;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

/** Activity to show the email details of a sent campaign that was selected **/
public class EmailDetailsActivity extends Activity 
								  implements OnClickListener {
	
	//email details activity menu items
	private final int MENU_REFRESH = 0;
	
	//intent for displaying the email details list of a sent campaign
	static final String ACTION_SHOW_EMAIL_LIST = 
		         "com.ctctlabs.quickview.EmailDetailsActivity.SHOW_EMAIL_LIST";
	
	//intent extras
	static final String EXTRA_CAMPAIGN_LINK = 
				 "com.ctctlabs.quickview.EmailDetailsActivity.extra.CAMPAIGN_LINK";
	static final String EXTRA_EVENT_TOTAL = 
				 "com.ctctlabs.quickview.EmailDetailsActivity.extra.DATA";
	static final int EXTRA_EVENT_TYPE = -1;
	
	private static final int OPENS = 0;
	private static final int CLICKS = 1;
	private static final int FORWARDS = 2;
	private static final int OPTOUTS = 3;
	private static final int SPAM = 4;
	private static final int BOUNCES = 5;
	
	//text views for the campaign name and the sent campaigns message
	private TextView tvCampaignName;
	private TextView tvSentCampaigns;
	
	//date campaign was sent
	private String date = "";
	
	//buttons for the open rate and click rate, if there are any
	private Button btnOpenRate;		private String opens;
	private Button btnClickRate;	private String clicks;
	
	//footer buttons for other data like forwards, spam, etc...
	private Button btnForwards;		private String forwards;
	private Button btnOptOuts;		private String optOuts;
	private Button btnSpam;			private String spam;
	private Button btnBounces;		private String bounces;
	
	//main ViewSwitcher for switching between the loading view and the main view
	private ViewSwitcher mSwitcher;
	private View mView; //main view
	
	//image view to display the graph
	private ImageView ivGraph;
	
	//main task for retrieving a single campaign
	private GetCampaignTask mTask;
	
	//the campaign link and the campaign to show details for
	private String campaignLink;
	private Campaign campaign;
	
	/* Setup the views */
	private void setupViews() {
		//inflate the email details view
		mView = View.inflate(this, 
				R.layout.email_details_view, 
				null);

		//setup the views for the campaign name and the sent campaigns
		tvCampaignName = (TextView)mView.findViewById(R.id.tv_campaignname);
		tvSentCampaigns = (TextView)mView.findViewById(R.id.tv_sent);
		
		//setup all the buttons for open rate, click rate, forwards, etc...
		btnOpenRate = (Button)mView.findViewById(R.id.btn_openrate);
		btnClickRate = (Button)mView.findViewById(R.id.btn_clickrate);
		btnForwards = (Button)mView.findViewById(R.id.btn_forwards);
		btnOptOuts = (Button)mView.findViewById(R.id.btn_optouts);
		btnSpam = (Button)mView.findViewById(R.id.btn_spam);
		btnBounces = (Button)mView.findViewById(R.id.btn_bounces);
		
		//graph for the most recent campaign
		ivGraph = (ImageView)mView.findViewById(R.id.piegraph_emaildetails);
		
		//setup the view switcher and add the two views
		mSwitcher = new ViewSwitcher(this);
	}//end setupViews
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//get rid of the title menu
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		//setup the views
		setupViews();
		
		//get passed data
		Campaign passedCampaign = (Campaign)getLastNonConfigurationInstance();
		if(passedCampaign == null) {
			
			//add the loading view 
			mSwitcher.addView(View.inflate(this, 
					R.layout.loading, 
					null));
			
			Intent intent = getIntent();
			if(intent.getAction() != null) {
				//get the campaign link for the campaign to display more data about
				campaignLink = getIntent()
							   .getStringExtra(EmailSummaryActivity.EXTRA_LINK);
			} else {
				Bundle extras = intent.getExtras();
				campaignLink = extras.getString("campaignLink");
			}//end else
			
			mTask = new GetCampaignTask();
			mTask.execute();
		} else {
			//set the campaign and the values
			campaign = passedCampaign;
			try {
				campaignLink = campaign.getAttribute("Link").toString();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			setValues();
		}//end else
		
		//add the main view and set the content view
		mSwitcher.addView(mView);
		setContentView(mSwitcher);

	}//end onCreate
	
	/* Pass the loaded campaign between orientations */
	@Override
	public Object onRetainNonConfigurationInstance() {
		//if the main task was running when orientation changed return null
		//so the activity is restarted and cancel the current thread
		if(mTask != null && mTask.getStatus() == AsyncTask.Status.RUNNING) {
			mTask.cancel(true);
			return null;
		}//end if
		return campaign;
	}//end onRetainNonConfigurationInstance

	/* Set values once the campaign has been retrieved */
	private void setValues() {
		if(campaign != null) {
			try {
				tvCampaignName.setText(campaign.getAttribute("Name").toString());
				
				//need bounces for  accurate sent campaigns stat
				bounces = campaign.getAttribute("Bounces").toString();
				String sent = campaign.getAttribute("Sent").toString();
				
				int sentInt = Integer.valueOf(sent) - Integer.valueOf(bounces);
				String actualSent = String.valueOf(sentInt);
				
				date = campaign.getAttribute("Date").toString();
				String formattedDate = QuickViewHelper.convertDate(date);
				tvSentCampaigns.setText(actualSent 
						+ (actualSent.equals("1") ? " email" : " emails") 
						+ " delivered on " + formattedDate);
				
				DecimalFormat formatter = new DecimalFormat("#0");
				float clickRate = 0, openRate = 0;
				
				opens = campaign.getAttribute("Opens").toString();
				int opensInt = Integer.valueOf(opens);
				if(opens.equals("0")) {
					btnOpenRate.setBackgroundColor(android.R.color.transparent);
				} else {
					openRate = EmailSummaryActivity
							   .calculateOpenRate(opensInt, sentInt);
					btnOpenRate.setText(formatter.format(openRate) + "% open rate");
					btnOpenRate.setOnClickListener(this);
				}//end if
				
				clicks = campaign.getAttribute("Clicks").toString();
				if(clicks.equals("0")) {
					btnClickRate.setBackgroundColor(android.R.color.transparent);
				} else {
					int clicksInt = Integer.valueOf(clicks);
					clickRate = EmailSummaryActivity
							    .calculateClickRate(clicksInt, opensInt);
					btnClickRate.setText(formatter.format(clickRate) + "% click rate");
					btnClickRate.setOnClickListener(this);
				}//end if
				
				forwards = campaign.getAttribute("Forwards").toString();
				if(forwards.equals("0")) {
					btnForwards.setBackgroundColor(android.R.color.transparent);
					btnForwards.setText("0\nforwards");
				} else {
					btnForwards.setText(forwards + "\nforwards");
					btnForwards.setOnClickListener(this);
				}//end else
				
				optOuts = campaign.getAttribute("OptOuts").toString();
				if(optOuts.equals("0")) {
					btnOptOuts.setBackgroundColor(android.R.color.transparent);
					btnOptOuts.setText("0\nopt-outs");
				} else {
					btnOptOuts.setText(optOuts + "\nopt-outs");
					btnOptOuts.setOnClickListener(this);
				}//end else
				
				//String spam = campaign.getAttribute("Spam").toString();
				//if(spam.equals("0")) {
					btnSpam.setBackgroundColor(android.R.color.transparent);
					btnSpam.setText("0\nspam");
				//} else {
				//	btnSpam.setText(spam + "\nspam");
				//}//end else
					
				if(bounces.equals("0")) {
					btnBounces.setBackgroundColor(android.R.color.transparent);
					btnBounces.setText("0\nbounces");
				} else {
					btnBounces.setText(bounces + "\nbounces");
					btnBounces.setOnClickListener(this);
				}//end else
				
				//if there are no opens, show an empty graph
				if(opensInt == 0)
					ivGraph.setImageResource(R.drawable.none);
				//otherwise create the graph image using the most recent data
				else
				{
					BitmapDrawable bg = (BitmapDrawable)getResources().getDrawable(R.drawable.background);
					Drawable layer = createPieChart(14f, 14f, 160, 160, bg, (openRate / 100f), (clickRate / 100f));
					ivGraph.setImageDrawable(layer);
				}
				
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			//show the next view
			mSwitcher.showNext();
			
		}//end if
	}//end setValues 

	private PieChart createPieChart(float left, float top,
									float right, float bottom,
									BitmapDrawable backgroundBitmap,
									float openPercentage, float clickPercentage) {
		RectF bounds = new RectF(left, top, right, bottom);
		PieChart chart = new PieChart(bounds, backgroundBitmap, openPercentage, clickPercentage);
		return chart;
	}//end createPieChart
	
	/* GetCampaignTask retrieves a single campaign in the background */
	private class GetCampaignTask extends AsyncTask<Void, Integer, 
	  									   		    Campaign> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}//end onPreExecute

		@Override
		protected Campaign doInBackground(Void... arg0) {
			try {
				campaign = Connection.getConn().getCampaign(campaignLink);
			} catch (InvalidCredentialsException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return campaign;
		}//end doInBackground

		@Override
		protected void onPostExecute(Campaign result) {
			super.onPostExecute(result);
			setValues();
		}//end onPostExecute
	}//end GetCampaignTask

	@Override
	public void onClick(View v)  {
		//new intent to show the contacts that performed a particular action
		Intent intent = null;
		
		//are the campaign stats old
		boolean isOld = is85DaysOld();
				
		//determine which action the user is interested in seeing
		switch(v.getId()) {
		case R.id.btn_openrate:
			if(isOld) {
				intent = new Intent(this, NoEmailDetails.class);
				intent.putExtra("EVENT_HEADING",  btnOpenRate.getText().toString() + 
												 ": (" + 
												 opens + 
												 (opens.equals("1") ? " contact)" : " contacts)"));
				startActivity(intent);
				return;
			} else {
				intent = new Intent(ACTION_SHOW_EMAIL_LIST);
				intent.putExtra("EVENT_TYPE", OPENS);
				intent.putExtra("EVENT_HEADING",  btnOpenRate.getText().toString() + 
						 ": (" + 
						 opens + 
						 (opens.equals("1") ? " contact)" : " contacts)"));
			}
			break;
			
		case R.id.btn_clickrate:
			if(isOld) {
				intent = new Intent(this, NoEmailDetails.class);
				intent.putExtra("EVENT_HEADING", btnClickRate.getText().toString() +
												 ": (" +
												 clicks +
												 (clicks.equals("1") ? " contact)" : " contacts)"));
				startActivity(intent);
				return;
			} else {
				intent = new Intent(ACTION_SHOW_EMAIL_LIST);
				intent.putExtra("EVENT_TYPE", CLICKS);
				intent.putExtra("EVENT_HEADING", btnClickRate.getText().toString() +
						 ": (" +
						 clicks +
						 (clicks.equals("1") ? " contact)" : " contacts)"));
			}
			break;
			
		case R.id.btn_forwards:
			if(isOld) {
				intent = new Intent(this, NoEmailDetails.class);
				intent.putExtra("EVENT_HEADING", "forwards: (" +
												 forwards +
												 (forwards.equals("1") ? " contact)" : " contacts)"));
				startActivity(intent);
				return;
			} else {
				intent = new Intent(ACTION_SHOW_EMAIL_LIST);
				intent.putExtra("EVENT_TYPE", FORWARDS);
				intent.putExtra("EVENT_HEADING", "forwards: (" +
						 forwards +
						 (forwards.equals("1") ? " contact)" : " contacts)"));
			}
			break;
			
		case R.id.btn_optouts:
			if(isOld) {
				intent = new Intent(this, NoEmailDetails.class);
				intent.putExtra("EVENT_HEADING", "opt-outs: (" +
												 optOuts +
												 (optOuts.equals("1") ? " contact)" : " contacts)"));
				startActivity(intent);
				return;
			} else {
				intent = new Intent(ACTION_SHOW_EMAIL_LIST);
				intent.putExtra("EVENT_TYPE", OPTOUTS);
				intent.putExtra("EVENT_HEADING", "opt-outs: (" +
						 optOuts +
						 (optOuts.equals("1") ? " contact)" : " contacts)"));
			}
			break;
			
		case R.id.btn_bounces:
			if(isOld) {
				intent = new Intent(this, NoEmailDetails.class);
				intent.putExtra("EVENT_HEADING", "bounces: (" +
												 bounces +
												 (bounces.equals("1") ? " contact)" : " contacts)"));
				startActivity(intent);
				return;
			} else {
				intent = new Intent(ACTION_SHOW_EMAIL_LIST);
				intent.putExtra("EVENT_TYPE", BOUNCES);
				intent.putExtra("EVENT_HEADING", "bounces: (" +
						 bounces +
						 (bounces.equals("1") ? " contact)" : " contacts)"));
			}
			break;
		}//end switch
		
		//and add the campaign link
		intent.putExtra("CAMPAIGN_LINK", campaignLink);
		
		//start the EmailDetailsListActivity activity
		startActivity(intent);
	}//end onClick
	
	private boolean is85DaysOld() {
		//ensure event tracking is even available - event details are no longer available via the API after 85 days from the send date.
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date todaysDate = new Date();
		Date campaignDate = null;
		try {
			campaignDate = formatter.parse(date);
		} catch (ParseException e) {
			//do nothing
		}
		
		long days = 0;
		if(campaignDate != null) {
			days = (todaysDate.getTime() - campaignDate.getTime()) / 86400000 ;
		}
		
		return days > 85 ? true : false;
	}//end is85DaysOld
	
	/* Create menu options */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    menu.add(0, MENU_REFRESH, 0, 
	    		 R.string.str_refreshmenu).setIcon(R.drawable.refresh_icon);       
        return true;
	}//end onCreateOptionsMenu
	
	/* Called when a menu item has been selected. */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
        	case MENU_REFRESH:
        		Intent intent = new Intent(this, EmailDetailsActivity.class);
        		intent.putExtra("campaignLink", campaignLink);
        		finish();
        		startActivity(intent);
        		return true;	
        }//end switch
		return false;
	}//end onOptionsItemSelected
}//end EmailDetailsActivity