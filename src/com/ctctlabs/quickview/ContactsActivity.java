
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

/**
 * @author Brendan White
 */

package com.ctctlabs.quickview;

import java.io.IOException;
import org.apache.http.auth.InvalidCredentialsException;
import org.apache.http.client.ClientProtocolException;

import com.ctctlabs.quickview.webservice.Contact;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

/** Activity to search for a contact and display the details. The menu items
 *  allow a user to logout and to add a new contact. **/
public class ContactsActivity extends Activity 
							  implements OnClickListener {
	
	//menu items
	private final int MENU_LOGOUT = 0;
	private final int MENU_ADD = 1;
	
	//intent variables for showing the contact details of a contact in the current list
	static final String ACTION_SHOW_CONTACT_DETAILS = 
						"com.ctctlabs.quickview.ContactsActivity.SHOW_CONTACT_DETAILS";
	static final String EXTRA_CONTACT_ID = 
						"com.ctctlabs.quickview.ContactsActivity.extra.CONTACT_ID";
	
	//contact to search for
	private EditText etContactToSearch;
	
	//task for searching contact
	private SearchContactsTask mSearchTask;
	
	/* Setup the views for the activity */
	private void setupViews() {
		//create the find contact button and set the listener
		Button btnFindContact = (Button)findViewById(R.id.btn_find_contact);
		btnFindContact.setOnClickListener(this);
		
		//create the clear contact button and set the listener
		Button btnClear = (Button)findViewById(R.id.btn_clear_contacts);
		btnClear.setOnClickListener(this);
		
		etContactToSearch = (EditText)findViewById(R.id.et_searchcontact);
	}//end setupViews
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//no title
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		//set the content view for the activity
		setContentView(R.layout.contacts_view);
		
		Boolean wasRunning = (Boolean) getLastNonConfigurationInstance();
		if(wasRunning != null && wasRunning) {
			showDialog(1);
		}//end if
		
		//setup the views
		setupViews();
	}//end onCreate
	
	@Override
	public Object onRetainNonConfigurationInstance() {
		Boolean isRunning = Boolean.FALSE;
		if(mSearchTask != null &&
				mSearchTask.getStatus() == AsyncTask.Status.RUNNING) {
			isRunning = Boolean.TRUE;
		}
		return isRunning;
	}//end onRetainNonConfigurationInstance

	/* SearchContactsTask */
	private class SearchContactsTask extends AsyncTask<String, Integer, 
											    	   Contact> {
		@Override
		protected Contact doInBackground(String... params) {
			//params[0] = email address to search
			Contact contact = null;
			try {
				contact = Connection.getConn().getContactByEmail(params[0]);
			} catch (InvalidCredentialsException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return contact;
		}//end doInBackground
		
		@Override
		protected void onCancelled() {
			super.onCancelled();
		}//end onCancelled

		@Override
		protected void onPostExecute(Contact contact) {
			super.onPostExecute(contact);

			String contactId = "";
			if(contact != null) { //found
				try {
					contactId = (String) contact.getAttribute("ContactId");
					} catch (ClientProtocolException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				showContactDetails(contactId);
			} else { //not found
				showContactNotFoundDialog();
			}//end if-else
			dismissDialog(1);
		}//end onPostExecute
	}//end SearchContactsTask
	
	private void showContactDetails(String contactId) {
		Intent intent = new Intent(ACTION_SHOW_CONTACT_DETAILS);
		intent.putExtra(EXTRA_CONTACT_ID, QuickViewHelper.getURLPath(contactId));
		startActivity(intent);		
	}//end showContactDetails
	
	private void showNoEmailDialog() {
		QuickViewHelper.showDialog(this, getString(R.string.str_errortitle), 
								   getString(R.string.str_noemailmsg), false);
	}//end isValidEmail
	
	private void showContactNotFoundDialog() {
		QuickViewHelper.showDialog(this, getString(R.string.str_contactnotfoundtitle), 
								   getString(R.string.str_contactnotfountmsg), false);
	}//end showContactNotFoundDialog
	
	@Override
	protected Dialog onCreateDialog(int id) {
		if(id == 1) {
			ProgressDialog mDialog = new ProgressDialog(this);
			mDialog.setMessage("Finding contact...");
			mDialog.setIndeterminate(true);
			mDialog.setCancelable(false);
			return mDialog;
		}
		
		return null;
	}//end onCreateDialog

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.btn_find_contact:
			String email = etContactToSearch.getText().toString();
			if(email.equals("")) {
				showNoEmailDialog();
			} else {
				showDialog(1);
				mSearchTask = new SearchContactsTask();
				mSearchTask.execute(email);
			}//end if-else
			break;
			
		case R.id.btn_clear_contacts:
			etContactToSearch.setText("");
			break;	
		}//end switch
	}//end onClick
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, MENU_LOGOUT, 0, "Log out")
			.setIcon(R.drawable.logout_icon);
		menu.add(0, MENU_ADD, 0, "Add Contact")
		.setIcon(android.R.drawable.ic_menu_add);
        return true;
	}//end onCreateOptionsMenu
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {       
		switch(item.getItemId()) {
        
    		case MENU_LOGOUT:
    			QuickViewHelper.showLogOutDialog(this, this);
    			return true;
		
			case MENU_ADD:
				startActivity(new Intent(this, AddContactActivity.class));
        		return true;          
        }//end switch   
		return false;
	}//end onOptionsItemSelected

	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return true;
		}//end if
		return super.onKeyDown(keyCode, event);
	}*/
}//end ContactsActivity
