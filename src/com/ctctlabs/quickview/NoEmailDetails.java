
/*
 * Copyright 1996-2009 Constant Contact, Inc.
 *   Licensed under the Apache License, Version 2.0 (the "License"); 
 *   you may not use this file except in compliance with the License. 
 *   You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0 
 *      
 *   Unless required by applicable law or agreed to in writing, software 
 *   distributed under the License is distributed on an "AS IS" BASIS, 
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *   See the License for the specific language governing permissions and 
 *   limitations under the License.
 */

/**
 * @author Brendan White
 */

package com.ctctlabs.quickview;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class NoEmailDetails extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
        //get the intent and the intent variables
		String headingText = "";
		Bundle extras = getIntent().getExtras();
		headingText = extras.getString("EVENT_HEADING");
		
		View mView = 
			 View.inflate(this, R.layout.emaildetailslist_unavailable, null);
		TextView headingTV = 
				(TextView) mView.findViewById(R.id.tv_emaildetailslist_unavailable_heading);
		headingTV.setText(headingText);

		setContentView(mView);
	}//end onCreate
}//end NoEmailDetails
